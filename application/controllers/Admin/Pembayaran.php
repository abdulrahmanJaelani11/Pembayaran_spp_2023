<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembayaran extends CI_Controller
{

	protected $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Pembayaran_model');
		is_login();
	}

	public function index()
	{
		$this->data['judul'] = 'Daftar Pembayaran';
		$this->data['pembayaran'] = $this->Pembayaran_model->getAllpembayaranPaid();

		$this->load->view('template/admin/header');
		$this->load->view('template/admin/sidebar', $this->data);
		$this->load->view('admin/vpembayaran', $this->data);
		$this->load->view('template/admin/footer');
	}

	public function form()
	{
		$this->data['judul'] = 'Form Pembayaran';

		if ($_POST) {
			$this->form_validation->set_rules('nisn', 'NISN', 'required|trim');
			$this->form_validation->set_rules('tanggal', 'Tanggal', 'required|trim');
			$this->form_validation->set_rules('nominal', 'Nominal Pembayaran', 'required|trim');

			if ($this->form_validation->run() == true) {
				$nisn = $this->input->post('nisn');
				$pembayaran = $this->input->post('pembayaran');
				$tanggal = $this->input->post('tanggal');
				$nominal = $this->input->post('nominal');
				$sisa_tagihan = $this->input->post('sisa_tagihan');
				$filter = ['nisn' => $nisn, 'id_pembayaran' => $pembayaran];

				$tagihan = $this->Pembayaran_model->getpembayaran($nisn, null, $pembayaran);

				if ($sisa_tagihan != '') {
					$nominal = $tagihan[0]->terbayar + $nominal;
				}

				if ($nominal >= $tagihan[0]->tagihan) {
					$status = 'paid';
				} else {
					$status = "pending";
				}

				$data = [
					'tgl_bayar' => $tanggal,
					'terbayar' => $nominal,
					'status' => $status,
					'metode_pembayaran' => 'cash',
					'order_id' => $this->randomPassword()
				];

				$this->Pembayaran_model->save($data, $filter);
				$this->session->set_flashdata('sukses', 'Berhasil membayar');
				redirect(base_url('Admin/Siswa/detail/' . $nisn));
			}
		}

		$this->load->view('template/admin/header');
		$this->load->view('template/admin/sidebar', $this->data);
		$this->load->view('admin/vpembayaran_form', $this->data);
		$this->load->view('template/admin/footer');
	}

	public function addPembayaran($data)
	{
		$this->Pembayaran_model->addPembayaran($data);
	}

	public function getPembayaran()
	{
		$id = $this->input->post('id');
		$nisn = $this->input->post('nisn');
		$data = $this->Pembayaran_model->getPembayaran($nisn, null, $id);
		echo json_encode($data);
	}

	public function ubah_tagihan()
	{
		$tagihan = $this->input->post('tagihan');

		$data = [
			'tagihan' => $tagihan
		];

		$this->Pembayaran_model->ubah_tagihan(null, $tagihan);
	}

	public function randomPassword()
	{
		$alphabet = '1234567890';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}
}

/* End of file Pembayaran.php and path \application\controllers\Admin\Pembayaran.php */
