<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelas extends CI_Controller
{

	protected $data;

	public function __construct()
	{
		parent::__construct();
		$this->data;
		$this->load->model('Kelas_model');
		is_login();
	}

	public function index()
	{
		$this->data['judul'] = "Daftar Kelas";


		$this->load->view('template/admin/header');
		$this->load->view('template/admin/sidebar', $this->data);
		$this->load->view('admin/vkelas', $this->data);
		$this->load->view('template/admin/footer');
	}

	public function form($id = null)
	{
		$this->data['judul'] = "Form Tambah Kelas";
		if ($id != null) {
			$this->data['judul'] = "Form Edit Kelas";
			$this->data['data_kelas'] = $this->Kelas_model->getKelas($id);
		}

		if ($_POST) {

			$data = [
				'kelas' => $this->input->post('kelas'),
				'created_at' => date("Y-d-m H:i:s"),
				'updated_at' => date("Y-d-m H:i:s"),
			];

			if ($id_kelas = $this->input->post('id')) {
				$update = $this->Kelas_model->update($id_kelas, $data);
				if ($update) {
					$this->session->set_flashdata('sukses', 'Berhasil merubah data kelas');
					redirect(base_url('Admin/Kelas'));
				} else {
					$this->session->set_flashdata('error', 'Gagal menambahkan kelas');
					redirect(base_url('Admin/Kelas'));
				}
			} else {
				$insert = $this->Kelas_model->save($data);
				if ($insert) {
					$this->session->set_flashdata('sukses', 'Berhasil menambahkan kelas');
					redirect(base_url('Admin/Kelas'));
				} else {
					$this->session->set_flashdata('error', 'Gagal menambahkan kelas');
					redirect(base_url('Admin/Kelas'));
				}
			}
		}

		$this->load->view('template/admin/header');
		$this->load->view('template/admin/sidebar', $this->data);
		$this->load->view('Admin/vkelas_form', $this->data);
		$this->load->view('template/admin/footer');
	}

	public function delete($id)
	{
		$this->Kelas_model->delete($id);
		$this->session->set_flashdata('sukses', 'Berhasil menghapus kelas');
		redirect(base_url('Admin/Kelas'));
	}
}

/* End of file Kelas.php and path \application\controllers\Kelas.php */
