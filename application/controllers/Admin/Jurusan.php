<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jurusan extends CI_Controller
{
	protected $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Jurusan_model');
		$this->load->model('Kelas_model');
		is_login();
	}

	public function index()
	{
		$this->data['judul'] = "Daftar jurusan";
		$this->data['jurusan'] = $this->Jurusan_model->getAllJurusan();
		$this->data['kelas'] = $this->Kelas_model->getAllKelas();

		$this->load->view('template/admin/header');
		$this->load->view('template/admin/sidebar', $this->data);
		$this->load->view('admin/vjurusan', $this->data);
		$this->load->view('template/admin/footer');
	}


	public function form($id = null)
	{
		$this->data['judul'] = "Form Tambah Jurusan";
		if ($id != null) {
			$this->data['judul'] = "Form Edit Jurusan";
			$this->data['jurusan'] = $this->Jurusan_model->getJurusan($id);
		}

		if ($_POST) {

			$data = [
				'jurusan' => $this->input->post('jurusan'),
				'singkatan' => $this->input->post('singkatan'),
				'created_at' => date("Y-d-m H:i:s"),
				'updated_at' => date("Y-d-m H:i:s"),
			];

			if ($this->input->post('id')) {
				$id = $this->input->post('id');
				$update = $this->Jurusan_model->update($id, $data);
				if ($update) {
					$this->session->set_flashdata('sukses', 'Berhasil mengubah data jurusan');
					redirect(base_url('Admin/Jurusan'));
				} else {
					$this->session->set_flashdata('error', 'Gagal mengubah data jurusan');
					redirect(base_url('Admin/Jurusan'));
				}
			} else {
				$insert = $this->Jurusan_model->save($data);
				if ($insert) {
					$this->session->set_flashdata('sukses', 'Berhasil menambahkan data jurusan');
					redirect(base_url('Admin/Jurusan'));
				} else {
					$this->session->set_flashdata('error', 'Gagal menambahkan data jurusan');
					redirect(base_url('Admin/Jurusan'));
				}
			}
		}

		$this->load->view('template/admin/header');
		$this->load->view('template/admin/sidebar', $this->data);
		$this->load->view('Admin/vjurusan_form', $this->data);
		$this->load->view('template/admin/footer');
	}

	function delete($id)
	{
		$delete = $this->Jurusan_model->delete($id);
		if ($delete) {
			$this->session->set_flashdata('sukses', 'Berhasil menghapus data jurusan');
			redirect(base_url('Admin/Jurusan'));
		}
	}
}

/* End of file Jurusan.php and path \application\controllers\Jurusan.php */
