<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Siswa extends CI_Controller
{

	protected $data;
	protected $kelas_model;

	public function __construct()
	{
		parent::__construct();
		$this->data;
		$this->load->model('Kelas_model');
		$this->load->model('Siswa_model');
		$this->load->model('Jurusan_model');
		$this->load->model('AkunSiswa_model');
		$this->load->model('Pembayaran_model');
		is_login();
	}

	public function index()
	{
		$this->data['judul'] = "Daftar Siswa";
		$this->data['siswa'] = $this->Siswa_model->getAllSiswa();

		$this->load->view('template/admin/header');
		$this->load->view('template/admin/sidebar', $this->data);
		$this->load->view('Admin/vsiswa', $this->data);
		$this->load->view('template/admin/footer');
	}

	public function getSiswa()
	{
		$nisn = $this->input->post('nisn');
		$siswa = $this->Siswa_model->find($nisn);
		$this->data['siswa'] = $siswa;
		$this->data['pembayaran'] = $this->Pembayaran_model->getPembayaran($nisn);
		echo json_encode($this->data);
	}

	function getTable($id_kelas = null)
	{
		$this->data['siswa'] = $this->Siswa_model->getSiswa($id_kelas);
		echo json_encode($this->load->view('Admin/table/tsiswa', $this->data));
	}

	function Cari()
	{
		$key = $this->input->post('key');
		$id = $this->input->post('id');
		$this->data['siswa'] = $this->Siswa_model->getPencarian($key, $id);
		echo json_encode($this->load->view('Admin/table/tsiswa', $this->data));
	}

	public function getSiswa_form()
	{
		$nisn = $this->input->post('nisn');
		$form = $this->input->post('form');
		$data = $this->Pembayaran_model->getPembayaran($nisn, $form);
		if (!isset($data['status'])) {
			echo '<option value="0">Pilih Bulan pembayaran</option>';
			foreach ($data as $row) {
				if ($row->index_bulan < date('m')) {
					echo '<option value="' . $row->id_pembayaran . '">' . $row->bulan . ' - ' . $row->tahun . '</option>';
				}
			}
		} else {
			echo "<option>Pembayaran Tidak Ditemukan<option>";
		}
	}

	public function form($nisn = null)
	{
		$this->data['judul'] = "Form Tambah Siswa";
		$this->data['kelas'] = $this->Kelas_model->getAllKelas();
		$this->data['jurusan'] = $this->Jurusan_model->getAllJurusan();

		if ($nisn != null) {
			$this->data['judul'] = "Form Ubah Siswa";
			$this->data['student'] = $this->Siswa_model->find($nisn);
		}

		if ($_POST) {

			$this->form_validation->set_rules('nama', 'Nama', 'required|trim');
			$this->form_validation->set_rules('nisn', 'NISN', 'required|trim');
			$this->form_validation->set_rules('kelas', 'Kelas', 'required|trim');
			$this->form_validation->set_rules('jurusan', 'jurusan', 'required|trim');
			$this->form_validation->set_rules('no_hp', 'No Handphone', 'required|trim');
			$this->form_validation->set_rules('tanggal_lahir', 'Tanggal lahir', 'required|trim');
			$this->form_validation->set_rules('tempat_lahir', 'Tempat lahir', 'required|trim');
			$this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');

			if ($this->form_validation->run() == true) {
				$data = [
					'nama' => $this->input->post('nama'),
					'nisn' => $this->input->post('nisn'),
					'jenis_kelamin' => $this->input->post('jenis_kelamin'),
					'agama' => $this->input->post('agama'),
					'nama_ayah' => $this->input->post('nama_ayah'),
					'nama_ibu' => $this->input->post('nama_ibu'),
					'berat' => $this->input->post('berat'),
					'tinggi' => $this->input->post('tinggi'),
					'golongan_darah' => $this->input->post('golongan_darah'),
					'riwayat_penyakit' => $this->input->post('riwayat_penyakit'),
					'id_kelas' => $this->input->post('kelas'),
					'id_jurusan' => $this->input->post('jurusan'),
					'no_hp' => $this->input->post('no_hp'),
					'tanggal_lahir' => $this->input->post('tanggal_lahir'),
					'tempat_lahir' => $this->input->post('tempat_lahir'),
					'alamat' => $this->input->post('alamat'),
					'created_at' => date("Y-d-m H:i:s"),
					'updated_at' => date("Y-d-m H:i:s"),
					'aktif' => 1
				];

				$id = 0;
				if ($this->input->post('id')) {
					$id_siswa = $this->input->post('id');
					$update = $this->Siswa_model->update_siswa($id_siswa, $data);
					if ($update) {
						$this->session->set_flashdata('sukses', 'Berhasil merubah data siswa');
						$nisn = $this->input->post('nisn');
						redirect(base_url('Admin/Siswa/detail/' . $nisn));
					} else {
						$this->session->set_flashdata('error', 'Gagal merubah data siswa');
						redirect(base_url('Admin/Siswa'));
					}
				} else {
					// Insert Data Siswa
					$id = $this->Siswa_model->save($data);

					// Insert data pembayaran Siswa
					$data_pembayaran = [
						[
							'nisn' => $this->input->post('nisn'),
							'tgl_bayar' => '',
							'bulan' => 'Januari',
							'index_bulan' => '01',
							'tahun' => date('Y'),
							'tagihan' => '100000',
							'index_bulan' => '01',
							'terbayar' => '',
							'status' => 'pending',
							'aktif' => 1
						],
						[
							'nisn' => $this->input->post('nisn'),
							'tgl_bayar' => '',
							'bulan' => 'Februari',
							'tahun' => date('Y'),
							'tagihan' => '100000',
							'index_bulan' => '02',
							'terbayar' => '',
							'status' => 'pending',
							'aktif' => 1
						],
						[
							'nisn' => $this->input->post('nisn'),
							'tgl_bayar' => '',
							'bulan' => 'Maret',
							'tahun' => date('Y'),
							'tagihan' => '100000',
							'index_bulan' => '03',
							'terbayar' => '',
							'status' => 'pending',
							'aktif' => 1
						],
						[
							'nisn' => $this->input->post('nisn'),
							'tgl_bayar' => '',
							'bulan' => 'April',
							'tahun' => date('Y'),
							'tagihan' => '100000',
							'index_bulan' => '04',
							'terbayar' => '',
							'status' => 'pending',
							'aktif' => 1
						],
						[
							'nisn' => $this->input->post('nisn'),
							'tgl_bayar' => '',
							'bulan' => 'Mei',
							'tahun' => date('Y'),
							'tagihan' => '100000',
							'index_bulan' => '05',
							'terbayar' => '',
							'status' => 'pending',
							'aktif' => 1
						],
						[
							'nisn' => $this->input->post('nisn'),
							'tgl_bayar' => '',
							'bulan' => 'Juni',
							'tahun' => date('Y'),
							'tagihan' => '100000',
							'index_bulan' => '06',
							'terbayar' => '',
							'status' => 'pending',
							'aktif' => 1
						],
						[
							'nisn' => $this->input->post('nisn'),
							'tgl_bayar' => '',
							'bulan' => 'Juli',
							'tahun' => date('Y'),
							'tagihan' => '100000',
							'index_bulan' => '07',
							'terbayar' => '',
							'status' => 'pending',
							'aktif' => 1
						],
						[
							'nisn' => $this->input->post('nisn'),
							'tgl_bayar' => '',
							'bulan' => 'Agustus',
							'tahun' => date('Y'),
							'tagihan' => '100000',
							'index_bulan' => '08',
							'terbayar' => '',
							'status' => 'pending',
							'aktif' => 1
						],
						[
							'nisn' => $this->input->post('nisn'),
							'tgl_bayar' => '',
							'bulan' => 'September',
							'tahun' => date('Y'),
							'tagihan' => '100000',
							'index_bulan' => '09',
							'terbayar' => '',
							'status' => 'pending',
							'aktif' => 1
						],
						[
							'nisn' => $this->input->post('nisn'),
							'tgl_bayar' => '',
							'bulan' => 'Oktober',
							'tahun' => date('Y'),
							'tagihan' => '100000',
							'index_bulan' => '10',
							'terbayar' => '',
							'status' => 'pending',
							'aktif' => 1
						],
						[
							'nisn' => $this->input->post('nisn'),
							'tgl_bayar' => '',
							'bulan' => 'November',
							'tahun' => date('Y'),
							'tagihan' => '100000',
							'index_bulan' => '11',
							'terbayar' => '',
							'status' => 'pending',
							'aktif' => 1
						],
						[
							'nisn' => $this->input->post('nisn'),
							'tgl_bayar' => '',
							'bulan' => 'Desember',
							'tahun' => date('Y'),
							'tagihan' => '100000',
							'index_bulan' => '12',
							'terbayar' => '',
							'status' => 'pending',
							'aktif' => 1
						],
					];

					$this->Pembayaran_model->addPembayaran($data_pembayaran);

					// Buat Akun siswa
					$data_akun = [
						'id_siswa' => $id,
						'username' => strtolower($data['nama']),
						'nisn' => $data['nisn'],
						'no_hp' => $data['no_hp'],
						'password' => $this->randomPassword()
					];

					$this->AkunSiswa_model->save($data_akun);
					if ($id) {
						$this->session->set_flashdata('sukses', 'Berhasil menambahkan siswa');
						$nisn = $this->input->post('nisn');
						redirect(base_url('Admin/Siswa/detail/' . $nisn));
					} else {
						$this->session->set_flashdata('error', 'Gagal menambahkan siswa');
						redirect(base_url('Admin/Siswa'));
					}
				}
			} else {
				$this->session->set_flashdata('error', 'Terjadi Kesalahan ketika menambahkan data, Silahka hubungi admin');
				redirect(base_url('Admin/Siswa/form'));
			}
		}

		$this->load->view('template/admin/header');
		$this->load->view('template/admin/sidebar', $this->data);
		$this->load->view('Admin/vsiswa_form', $this->data);
		$this->load->view('template/admin/footer');
	}

	public function randomPassword()
	{
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}

	public function import_excel()
	{
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		$config['upload_path'] = realpath('excel');
		$config['allowed_types'] = 'xlsx|xls|csv';
		$config['max_size'] = '10000';
		$config['encrypt_name'] = true;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload()) {
			echo 'Errors';
		} else {
			$data_upload = $this->upload->data();

			$excelreader = new PHPExcel_Reader_Excel2007();
			$loadExcel = $excelreader->load('excel/' . $data_upload['file_name']);
			$sheet = $loadExcel->getActiveSheet()->toArray(null, true, true, true);

			$data = array();

			$i = 1;
			foreach ($sheet as $row) {
				if ($i > 1) {
					array_push($data, array(
						'nama' => $row['A'],
						'id_kelas' => $row['B'],
						'id_jurusan' => $row['C'],
						'nisn' => $row['D'],
						'tanggal_lahir' => $row['E'],
						'tempat_lahir' => $row['F'],
						'no_hp' => $row['G'],
						'alamat' => $row['H'],
					));
				}
				$i++;
			}

			$this->db->insert_batch('ref_siswa', $data);

			unlink(realpath('excel/' . $data_upload['file_name']));
			echo 'SUKSES';
		}
	}

	public function kelas($id_kelas)
	{
		$this->data['judul'] = "Daftar Siswa";
		$this->data['siswa'] = $this->Siswa_model->getSiswa($id_kelas);
		$this->data['kelas'] = $this->Kelas_model->getAllKelas();
		$this->data['single_kelas'] = $this->Kelas_model->getKelas($id_kelas);
		// echo "<pre>";
		// var_dump($this->data['siswa']);
		// echo "</pre>";
		// die;

		$this->load->view('template/admin/header');
		$this->load->view('template/admin/sidebar', $this->data);
		$this->load->view('Admin/vsiswa', $this->data);
		$this->load->view('template/admin/footer');
	}

	public function detail($nisn)
	{
		$this->data['judul'] = "Rincian Siswa";
		$this->data['siswa'] = $this->Siswa_model->find($nisn);
		$this->data['pembayaran'] = $this->Pembayaran_model->getAllPembayaran($nisn);
		$this->data['jurusan'] = $this->Jurusan_model->getAllJurusan();
		$this->data['akun'] = $this->AkunSiswa_model->getAkun($nisn);

		$this->load->view('template/admin/header');
		$this->load->view('template/admin/sidebar', $this->data);
		$this->load->view('Admin/vsiswa_det', $this->data);
		$this->load->view('template/admin/footer');
	}

	public function delete($id)
	{
		if ($this->Siswa_model->delete_siswa($id)) {
			$this->session->set_flashdata('sukses', 'Berhasil Menghapus data siswa');
			redirect(base_url('Admin/siswa'));
		} else {
			echo 'GAGAL';
		}
	}

	public function profil()
	{
		$nisn = $this->session->userdata('nisn');
		$siswa = $this->Siswa_model->find($nisn);
		$this->data['siswa'] = $siswa;
		$this->load->view('template/siswa/header');
		$this->load->view('template/siswa/sidebar', $this->data);
		$this->load->view('siswa/vprofil', $this->data);
		$this->load->view('template/admin/footer');
	}
}
