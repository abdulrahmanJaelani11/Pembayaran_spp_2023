<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}

	public function index()
	{
		if ($this->session->userdata('email')) {
			redirect(base_url('Admin/Dashboard'));
		} else {
			if ($this->session->userdata('nisn')) {
				redirect(base_url('Siswa/Dashboard'));
			}
			$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email', ['required' => 'Email harus diisi!']);
			$this->form_validation->set_rules('password', 'Password', 'required|trim', ['required' => 'Password harus diisi!']);

			if ($this->form_validation->run() == false) {
				$this->load->view('template/auth/header');
				$this->load->view('admin/vlogin');
				$this->load->view('template/auth/footer');
			} else {
				$this->login();
			}
		}
	}

	private function login()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$user = $this->db->get_where('users', ['email' => $email])->row_array();
		if ($user) {
			if ($user['aktif'] == 1) {
				if (password_verify($password, $user['password'])) {
					$session = [
						'email' => $user['email'],
						'role_id' => $user['role_id']
					];

					$this->session->set_userdata($session);
					redirect(base_url('Admin/Dashboard'));
				} else {
					$this->session->set_flashdata('error', 'Email atau password yang anda masukan salah!');
					redirect(base_url('Admin/Auth'));
				}
			} else {
				$this->session->set_flashdata('error', 'Maaf akun belum aktif!');
				redirect(base_url('Admin/Auth'));
			}
		} else {
			$this->session->set_flashdata('error', 'Email Tidak Terdaftar!');
			redirect(base_url('Admin/Auth'));
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('role_id');
		redirect(base_url('Admin/Auth'));
	}
}

/* End of file Auth.php and path \application\controllers\Admin\Auth.php */
