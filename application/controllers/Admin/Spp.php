<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Spp extends CI_Controller
{
    protected $data;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Spp_model');
        $this->load->model('Kelas_model');
    }

    public function index()
    {
        $this->data['judul'] = "Ubah Biaya SPP";
        $this->data['spp'] = $this->Spp_model->getAllSpp();
        $this->data['kelas'] = $this->Kelas_model->getAllKelas();

        $this->load->view('template/admin/header');
        $this->load->view('template/admin/sidebar', $this->data);
        $this->load->view('admin/vspp', $this->data);
        $this->load->view('template/admin/footer');
    }


    public function form()
    {

        $this->data['judul'] = "Form Tambah Jurusan";

        if ($_POST) {

            $data = [
                'tahun' => $this->input->post('tahun'),
                'nominal' => $this->input->post('nominal'),
                'aktif' => 1,
            ];

            $id = $this->Spp_model->save($data);
            if ($id) {
                $this->session->set_flashdata('sukses', 'Berhasil Menambahkan SPP');
                redirect(base_url('Admin/Spp'));
            } else {
                $this->session->set_flashdata('error', 'Gagal Menambahkan SPP');
                redirect(base_url('Admin/Spp'));
            }
            die;
        }

        $this->load->view('template/admin/header');
        $this->load->view('template/admin/sidebar', $this->data);
        $this->load->view('Admin/vspp_form', $this->data);
        $this->load->view('template/admin/footer');
    }
}

/* End of file Jurusan.php and path \application\controllers\Jurusan.php */
