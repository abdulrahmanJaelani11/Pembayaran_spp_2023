<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Kelas_model');
		$this->load->model('Siswa_model');
		is_login();
	}

	public function index()
	{
		$this->data['kelas'] = $this->Kelas_model->getAllKelas();
		$jumlah_siswa = $this->Siswa_model->getAllSiswa();
		$this->data['jml_pria'] = count($this->Siswa_model->getSiswaJk(['aktif' => 1, 'jenis_kelamin' => 'Laki-laki']));
		$this->data['jml_wnt'] = count($this->Siswa_model->getSiswaJk(['aktif' => 1, 'jenis_kelamin' => 'Perempuan']));
		$jumlah_siswa = $jumlah_siswa;
		$this->data['jumlah_siswa'] = count($jumlah_siswa);

		$this->load->view('template/admin/header');
		$this->load->view('template/admin/sidebar', $this->data);
		$this->load->view('Admin/dashboard', $this->data);
		$this->load->view('template/admin/footer');
	}
}

/* End of file Dashboard.php and path \application\controllers\Admin\Dashboard.php */
