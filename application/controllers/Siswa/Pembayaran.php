<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembayaran extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Pembayaran_model');
		$this->load->model('Siswa_model');
		is_login_siswa();
	}

	public function index()
	{
		$nisn = $this->session->userdata('nisn');
		$this->data['pembayaran_bulan_ini'] = $this->Pembayaran_model->pembayaranBulanIni($nisn);
		$this->data['terbayar'] = $this->Pembayaran_model->terbayar($nisn);
		$this->data['belum_terbayar'] = $this->Pembayaran_model->getPemBelumTerbayar($nisn, true);
		$this->data['siswa'] = $this->Siswa_model->find($nisn);
		$nominal_terbayar = 0;
		foreach ($this->data['terbayar'] as $row) {
			$nominal_terbayar += $row->terbayar;
		}

		$this->data['nominal__terbayar'] = $nominal_terbayar;

		$this->load->view('template/siswa/header');
		$this->load->view('template/siswa/sidebar', $this->data);
		$this->load->view('siswa/vpembayaran', $this->data);
		$this->load->view('template/siswa/footer');
	}

	public function addPembayaran()
	{
		$id_pem = $this->input->post('id');
		$nisn = $this->input->post('nisn');
		$tagihan = $this->input->post('tagihan');
		$orderid = $this->input->post('orderid');
		$transaksi_id = $this->input->post('transaksi_id');
		$kode_pembayaran = $this->input->post('kode_pembayaran');
		$mandiri_kode = $this->input->post('mandiri_kode');
		$bank = $this->input->post('bank');
		$metode_pembayaran = $this->input->post('metode_pembayaran');
		$tanggal_transaksi = $this->input->post('tanggal_transaksi');
		$status = $this->input->post('status');
		$data_update = [
			'tagihan' => $tagihan,
			'order_id' => $orderid,
			'transaksi_id' => $transaksi_id,
			'kode_pembayaran' => $kode_pembayaran,
			'mandiri_kode' => $mandiri_kode,
			'bank' => $bank,
			'metode_pembayaran' => $metode_pembayaran,
			'tgl_bayar' => date('Y-m-d'),
			'status' => $status
		];

		if ($this->Pembayaran_model->update($id_pem, $data_update)) {
			$this->session->set_flashdata('sukses', 'Segera Lakukan Pembayaran!');
			redirect(base_url('Siswa/Pembayaran/history_det/' . $id_pem));
		} else {
			echo 'GAGAL';
		}
		// $data = $this->Pembayaran_model->update($id_pem, $data);

	}

	function history()
	{
		$nisn = $this->session->userdata('nisn');
		$this->data['list_pembayaran'] = $this->Pembayaran_model->getAllPembayaran($nisn, true);
		$this->load->view('template/siswa/header');
		$this->load->view('template/siswa/sidebar', $this->data);
		$this->load->view('siswa/vhistory', $this->data);
		$this->load->view('template/siswa/footer');
	}

	function history_det($id_pem)
	{
		$nisn = $this->session->userdata('nisn');
		$this->data['pembayaran'] = $this->Pembayaran_model->getPembayaran($nisn, null, $id_pem);

		$this->load->view('template/siswa/header');
		$this->load->view('template/siswa/sidebar', $this->data);
		$this->load->view('siswa/vhistory_det', $this->data);
		$this->load->view('template/siswa/footer');
	}

	public function bayar()
	{
		echo 'OK';
		die;
		$json_result = file_get_contents('php://input');
		$result = json_decode($json_result);

		$data = [
			"status" => "paid",
			"terbayar" => $result->gross_amount,
		];
		if ($result->status_code == 200) {
			$this->db->update('pembayaran', $data, ['order_id' => $result->order_id]);
		}
	}

	public function getPembayaran()
	{
		$id_pem = $this->input->post('id_pembayaran');
		$nisn = $this->input->post('nisn');

		$data = $this->Pembayaran_model->getPembayaran($nisn, null, $id_pem);
		echo json_encode($data);
	}
}

/* End of file Siswa.php and path \application\controllers\Siswa\Siswa.php */
