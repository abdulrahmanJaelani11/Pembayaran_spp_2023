<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Siswa_model');
		$this->load->model('Pembayaran_model');
		is_login_siswa();
	}

	public function index()
	{
		$nominal_terbayar = 0;
		$bulan_terbayar = 0;
		$belum_terbayar = 0;
		$nisn = $this->session->userdata('nisn');

		$this->data['siswa'] = $this->Siswa_model->find($nisn);
		$this->data['terbayar'] = $this->Pembayaran_model->terbayar($nisn);
		$this->data['belum_terbayar'] = $this->Pembayaran_model->getPemBelumTerbayar($nisn);

		foreach ($this->data['terbayar'] as $row) {
			$nominal_terbayar += $row->terbayar;
			$bulan_terbayar += 1;
		}
		foreach ($this->data['belum_terbayar'] as $row) {
			$belum_terbayar += $row->tagihan;
		}

		$this->data['bulan__terbayar'] = $bulan_terbayar;
		$this->data['nominal__terbayar'] = $nominal_terbayar;
		$this->data['nominal_belum_terbayar'] = $belum_terbayar;

		$this->load->view('template/siswa/header');
		$this->load->view('template/siswa/sidebar', $this->data);
		$this->load->view('Siswa/dashboard', $this->data);
		$this->load->view('template/siswa/footer');
	}
}

/* End of file Dashboard.php and path \application\controllers\Admin\Dashboard.php */
