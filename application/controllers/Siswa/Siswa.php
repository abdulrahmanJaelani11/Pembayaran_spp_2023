<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Siswa extends CI_Controller
{

    private $nisn;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Pembayaran_model');
        $this->load->model('AkunSiswa_model');
        $this->nisn = $this->session->userdata('nisn');
    }

    public function profil()
    {
        $this->data['pembayaran_bulan_ini'] = $this->Pembayaran_model->pembayaranBulanIni($this->nisn);
        $this->load->view('template/siswa/header');
        $this->load->view('template/siswa/sidebar', $this->data);
        $this->load->view('Siswa/vprofil', $this->data);
        $this->load->view('template/siswa/footer');
    }
}

/* End of file Siswa.php and path \application\controllers\Siswa\Siswa.php */
