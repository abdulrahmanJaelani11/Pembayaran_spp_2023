<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    private $nisn;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Pembayaran_model');
        $this->load->model('AkunSiswa_model');
        $this->nisn = $this->session->userdata('nisn');
    }

    public function index()
    {
        if ($this->session->userdata('nisn')) {
            redirect(base_url('Siswa/Dashboard'));
        } else {
            if ($this->session->userdata('email')) {
                redirect(base_url('Admin/Dashboard'));
            }
            $this->form_validation->set_rules('nisn', 'NISN', 'required|trim', ['required' => 'NISN harus diisi!']);
            $this->form_validation->set_rules('password', 'Password', 'required|trim', ['required' => 'Silahkan masukan password terlebih dulu']);

            if ($this->form_validation->run() == false) {
                $this->load->view('template/auth/header');
                $this->load->view('Siswa/vlogin');
                $this->load->view('template/auth/footer');
            } else {
                $this->login();
            }
        }
    }

    private function login()
    {
        $nisn = $this->input->post('nisn');
        $password = $this->input->post('password');

        $siswa = $this->db->get_where('ref_akun_siswa', ['nisn' => $nisn])->row_array();
        if ($siswa) {
            if ($siswa['aktif'] == 1) {
                if (strlen($siswa['password']) > 20) {
                    if (password_verify($password, $siswa['password'])) {
                        $session = [
                            'nisn' => $siswa['nisn'],
                        ];

                        $this->session->set_userdata($session);
                        redirect(base_url('Siswa/Dashboard'));
                    } else {
                        $this->session->set_flashdata('error', 'NISN atau password yang anda masukan salah!');
                        redirect(base_url('Siswa/Auth'));
                    }
                } else {

                    if ($password == $siswa['password']) {
                        $session = [
                            'nisn' => $siswa['nisn'],
                        ];

                        $this->session->set_userdata($session);
                        redirect(base_url('Siswa/Dashboard'));
                    } else {
                        $this->session->set_flashdata('error', 'NISN atau password yang anda masukan salah!');
                        redirect(base_url('Siswa/Auth'));
                    }
                }
            } else {
                $this->session->set_flashdata('error', 'Maaf akun belum aktif!');
                redirect(base_url('Siswa/Auth'));
            }
        } else {
            $this->session->set_flashdata('error', 'NISN Tidak Terdaftar!');
            redirect(base_url('Siswa/Auth'));
        }
    }

    function logout()
    {
        $this->session->unset_userdata('nisn');
        redirect(base_url('Siswa/Auth'));
    }

    public function ubah_password()
    {
        $password_sekarang = $this->input->post('password_sekarang');
        $password_baru = $this->input->post('password_baru');
        $konfirmasi_password = $this->input->post('password_konfirmasi');

        $akun_siswa = $this->AkunSiswa_model->getAkun($this->nisn);

        if (strlen($akun_siswa->password) > 20) {
            if (password_verify($password_sekarang, $akun_siswa->password)) {
                if ($password_baru == $konfirmasi_password) {
                    $data = [
                        'password' => password_hash($password_baru, PASSWORD_BCRYPT)
                    ];
                    if ($this->AkunSiswa_model->update($this->nisn, $data)) {
                        $this->session->unset_userdata('nisn');
                        $this->session->set_flashdata('sukses', 'Berhasil mengubah password');
                        redirect('Siswa/Auth');
                    }
                } else {
                    echo 'Tidak sama';
                }
            } else {
                echo 'SALAH';
            }
        } else {
            if ($akun_siswa->password == $password_sekarang) {
                if ($password_baru == $konfirmasi_password) {
                    $data = [
                        'password' => password_hash($password_baru, PASSWORD_BCRYPT)
                    ];
                    if ($this->AkunSiswa_model->update($this->nisn, $data)) {
                        $this->session->unset_userdata('nisn');
                        $this->session->set_flashdata('sukses', 'Berhasil mengubah password');
                        redirect('Siswa/Auth');
                    }
                } else {
                    echo 'Tidak sama';
                }
            } else {
                echo 'SALAH';
            }
        }
    }
}

/* End of file Auth.php and path \application\controllers\Siswa\Auth.php */
