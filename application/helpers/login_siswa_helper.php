<?php

function is_login_siswa()
{
    $ci = get_instance();
    if (!$ci->session->userdata('nisn')) {
        redirect(base_url('Siswa/Auth'));
    }
}
