<?php

function is_login()
{
    $ci = get_instance();
    if (!$ci->session->userdata('email')) {
        redirect(base_url('Admin/Auth'));
    }
}
