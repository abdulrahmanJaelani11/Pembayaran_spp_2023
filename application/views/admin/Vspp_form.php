<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title"><?= $judul ?></h4>
                            <!-- <p class="card-description"> Basic form elements </p> -->
                            <form class="forms-sample" action="<?= base_url() ?>Admin/Spp/form" method="POST">
                                <div class="form-group">
                                    <label for="tahun">Tahun</label>
                                    <input type="text" class="form-control" name="tahun" id="tahun" placeholder="Tahun">
                                </div>
                                <div class="form-group">
                                    <label for="nominal">Nominal</label>
                                    <input type="text" class="form-control" name="nominal" id="nominal" placeholder="Nominal">
                                </div>
                                <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                                <a href="<?= base_url('Admin/Spp') ?>" class="btn btn-light">Kembali</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>