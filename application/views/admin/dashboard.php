<div class="row">
    <div class="col-12">
        <h3 class="text-dark">Selamat Datang <?= $user[0]->username ?></h3>
        <div class="row">
            <div class="col-xl-3 col-lg-6 col-sm-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="mb-2 text-dark font-weight-normal">Jumlah Siswa</h5>
                        <i class="mdi mdi-account-multiple text-dark" style="font-size: 25px;"></i>
                        <h2 class="mb-4 text-dark font-weight-bold"><?= $jumlah_siswa; ?></h2>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-sm-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="mb-2 text-dark font-weight-normal">Jumlah Siswa Laki-Laki</h5>
                        <i class="mdi mdi-human-male text-dark" style="font-size: 25px;"></i>
                        <h2 class="mb-4 text-dark font-weight-bold"><?= $jml_pria; ?></h2>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-sm-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="mb-2 text-dark font-weight-normal">Jumlah Siswa Perempuan</h5>
                        <i class="mdi mdi-human-female text-dark" style="font-size: 25px;"></i>
                        <h2 class="mb-4 text-dark font-weight-bold"><?= $jml_wnt; ?></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Jumlah Siswa</h4>
                <canvas id="barChart" style="height:230px"></canvas>
            </div>
        </div>
    </div>
</div>