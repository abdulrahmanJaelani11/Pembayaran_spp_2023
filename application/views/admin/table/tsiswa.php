<table class="table table-bordered">
    <thead>
        <tr>
            <th> No. </th>
            <th> Nama </th>
            <th> NISN </th>
            <th> Kelas </th>
            <th> Jurusan </th>
            <th> Tanggal Lahir </th>
            <th> Tempat Lahir </th>
            <th class="text-center"> # </th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1 ?>
        <?php foreach ($siswa as $row) : ?>
            <tr>
                <td><?= $no++ ?></td>
                <td><?= $row->nama ?></td>
                <td><?= $row->nisn ?></td>
                <td><?= $row->kelas ?></td>
                <td><?= $row->jurusan . ' (' . $row->singkatan . ')' ?></td>
                <td><?= $row->tanggal_lahir ?></td>
                <td><?= $row->tempat_lahir ?></td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Options </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton5" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 39px, 0px);">
                            <a class="dropdown-item" href="<?= base_url('Admin/Siswa/detail/' . $row->nisn); ?>">Rincian</a>
                            <a class="dropdown-item" href="<?= base_url('Admin/siswa/delete/' . $row->id); ?>">Hapus</a>
                            <a class="dropdown-item" href="<?= base_url('Admin/Siswa/form/' . $row->nisn); ?>">Edit</a>
                        </div>
                    </div>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>