<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="row">
	<div class="col-lg-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?= $judul ?></h4>

				<!-- ALERT -->
				<?php if ($this->session->flashdata('sukses')) : ?>
					<div class="alert alert-success">
						<?= $_SESSION['sukses']; ?>
					</div>
				<?php endif ?>
				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger">
						<?= $_SESSION['error']; ?>
					</div>
				<?php endif ?>

				<div class="row">
					<div class="col-md-12">
						<div class="d-sm-flex justify-content-between align-items-center transaparent-tab-border {">
							<ul class="nav nav-tabs tab-transparent" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="siswa-det-tab" data-toggle="tab" href="#siswa-detail" role="tab" aria-selected="true">Rincian Siswa</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="business-tab" data-toggle="tab" href="#business-1" role="tab" aria-selected="false">Rincian Pembayaran</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="edit-siswa-tab" data-toggle="tab" href="#edit-siswa" role="tab" aria-selected="false">Edit Siswa</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="akun-tab" data-toggle="tab" href="#akun" role="tab" aria-selected="false">Akun Siswa</a>
								</li>
							</ul>
							<div class="d-md-block d-none">
								<a href="#" class="text-light p-1"><i class="mdi mdi-view-dashboard"></i></a>
								<a href="#" class="text-light p-1"><i class="mdi mdi-dots-vertical"></i></a>
							</div>
						</div>
						<div class="tab-content tab-transparent-content">
							<div class="tab-pane fade show active" id="siswa-detail" role="tabpanel" aria-labelledby="siswa-det-tab">
								<div class="row">
									<div class="col-lg-8">
										<div class="card shadow-sm">
											<div class="card-body">
												<div class="row">
													<div class="col-lg-4">
														<img src="<?= base_url('assets_me/images/profile/default.png'); ?>" alt="Profil" class="img-fluid img-thumbnail">
													</div>
													<div class="col-lg-7">
														<div class="row">
															<div class="col-12">
																<p style="margin: 0; font-size: 25px; font-weight: bold;" class="text-dark"><?= $siswa->nama; ?></p>
																<hr>
															</div>
															<div class="col-12">
																<p style="margin: 0; font-size: 18px;" class="text-dark"><?= $siswa->kelas; ?></p>
																<hr>
															</div>
															<div class="col-12">
																<p style="margin: 0; font-size: 18px;" class="text-dark"><?= $siswa->jurusan; ?></p>
																<hr>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade show" id="business-1" role="tabpanel" aria-labelledby="business-tab">
								<div class="row">
									<div class="col-12 mt-4">
										<h3 class="text-dark">Pembayaran</h3>
										<table class="table table-bordered">
											<thead>
												<tr>
													<th> No. </th>
													<th> Bulan </th>
													<th> Tahun </th>
													<th> Tagihan </th>
													<th> Terbayar </th>
													<th> Status </th>
													<th class="text-center"> # </th>
												</tr>
											</thead>
											<tbody>
												<?php $no = 1 ?>
												<?php foreach ($pembayaran as $row) : ?>
													<tr>
														<td><?= $no++ ?></td>
														<td><?= $row->bulan ?></td>
														<td><?= $row->tahun ?></td>
														<td>Rp.<?= number_format($row->tagihan, 0, 0, '.') ?></td>
														<td><?= $row->terbayar != '' ? 'Rp.' . number_format($row->terbayar, 0, 0, '.') : '' ?></td>
														<td class="text-center">
															<label class="badge <?= $row->status === 'paid' ? 'badge-success' : 'badge-danger'; ?>"><?= $row->status === 'paid' ? 'Lunas' : 'Belum Lunas' ?></label>
														</td>
														<td class="text-center">
															<div class="dropdown">
																<button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Options </button>
																<div class="dropdown-menu" aria-labelledby="dropdownMenuButton5" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 39px, 0px);">
																	<a class="dropdown-item" href="<?= base_url('Admin/Siswa/detail/' . $row->nisn); ?>">Rincian</a>
																	<a class="dropdown-item" href="<?= base_url('Admin/siswa/delete/' . $row->id_pembayaran); ?>">Hapus</a>
																	<a class="dropdown-item" href="<?= base_url('Admin/Siswa/form/' . $row->nisn); ?>">Edit</a>
																</div>
															</div>
														</td>
													</tr>
												<?php endforeach ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="tab-pane fade show" id="edit-siswa" role="tabpanel" aria-labelledby="edit-siswa-tab">
								<div class="row justify-content-center">
									<div class="col-lg-10 mt-4">
										<div class="card shadow">
											<div class="card-body">
												<h3 class="text-dark">Form Edit Siswa</h3>
												<form class="forms-sample" action="<?= base_url() ?>Admin/Siswa/form" method="POST">
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label for="nama">Nama</label>
																<input type="hidden" name="id" id="id" value="<?= isset($siswa) ? $siswa->id : ''; ?>">
																<input type="text" class="form-control" value="<?= isset($siswa) ? $siswa->nama :  set_value('nama'); ?>" name="nama" id="nama" placeholder="Nama">
																<?= form_error('nama', '<small class="text-danger">', '</small>'); ?>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label for="nisn">NISN</label>
																<input type="text" class="form-control" name="nisn" id="nisn" placeholder="NISN" value="<?= isset($siswa) ? $siswa->nisn :  set_value('nisn'); ?>">
																<?= form_error('nisn', '<small class="text-danger">', '</small>'); ?>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="form-group">
																<label for="jenis_kelamin">Jenis Kelamin</label>
																<select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
																	<option value="">- Pilih Jenis Kelamin -</option>
																	<option value="Laki-laki" <?= isset($siswa) ? ($siswa->jenis_kelamin == 'Laki-laki' ? 'selected' : '') : ''; ?>>Laki-laki</option>
																	<option value="Perempuan" <?= isset($siswa) ? ($siswa->jenis_kelamin == 'Perempuan' ? 'selected' : '') : ''; ?>>Perempuan</option>
																</select>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label for="kelas">Kelas</label>
																<select class="form-control" name="kelas" id="kelas">
																	<option value="">-- Pilih Kelas --</option>
																	<?php foreach ($kelas as $row) : ?>
																		<option <?= isset($siswa) ? ($siswa->id_kelas == $row->id_kelas ? 'selected' : '')  : ''; ?> value="<?= $row->id_kelas ?>"><?= $row->kelas ?></option>
																	<?php endforeach ?>
																</select>
																<?= form_error('kelas', '<small class="text-danger">', '</small>'); ?>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label for="jurusan">Jurusan</label>
																<select class="form-control" name="jurusan" id="jurusan">
																	<option value="">-- Pilih Jurusan --</option>
																	<?php foreach ($jurusan as $row) : ?>
																		<option <?= isset($siswa) ? ($siswa->id_jurusan == $row->id_jurusan ? 'selected' : '')  : ''; ?> value="<?= $row->id_jurusan ?>"><?= $row->jurusan ?></option>
																	<?php endforeach ?>
																</select>
																<?= form_error('jurusan', '<small class="text-danger">', '</small>'); ?>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label for="no_hp">No HP</label>
																<input type="number" class="form-control" name="no_hp" id="no_hp" placeholder="No. HP" value="<?= isset($siswa) ? $siswa->no_hp :  set_value('no_hp'); ?>">
																<?= form_error('no_hp', '<small class="text-danger">', '</small>'); ?>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label for="tanggal_lahir">Tanggal Lahir</label>
																<input type="date" class="form-control" name="tanggal_lahir" id="tanggal_lahir" placeholder="Tanggal Lahir" value="<?= isset($siswa) ? $siswa->tanggal_lahir :  set_value('tanggal_lahir'); ?>">
																<?= form_error('tanggal_lahir', '<small class="text-danger">', '</small>'); ?>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label for="tempat_lahir">Tempat Lahir</label>
																<input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir" value="<?= isset($siswa) ? $siswa->tempat_lahir :  set_value('tempat_lahir'); ?>">
																<?= form_error('tempat_lahir', '<small class="text-danger">', '</small>'); ?>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label for="agama">Agama</label>
																<select name="agama" id="agama" class="form-control">
																	<option value="">- Pilih agama -</option>
																	<option value="islam" <?= isset($siswa) ? ($siswa->agama == 'islam' ? 'selected' : '') : ''; ?>>Islam</option>
																	<option value="katholik" <?= isset($siswa) ? ($siswa->agama == 'katholik' ? 'selected' : '') : ''; ?>>Katholik</option>
																	<option value="hindu" <?= isset($siswa) ? ($siswa->agama == 'hindu' ? 'selected' : '') : ''; ?>>Hindu</option>
																	<option value="budha" <?= isset($siswa) ? ($siswa->agama == 'budha' ? 'selected' : '') : ''; ?>>Budha</option>
																	<option value="konghucu" <?= isset($siswa) ? ($siswa->agama == 'konghucu' ? 'selected' : '') : ''; ?>>Konghucu</option>
																	<option value="kristen" <?= isset($siswa) ? ($siswa->agama == 'kristen' ? 'selected' : '') : ''; ?>>Kristen</option>
																</select>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label for="nama_ayah">Nama Ayah</label>
																<input type="text" class="form-control" name="nama_ayah" id="nama_ayah" placeholder="Nama Ayah" value="<?= isset($siswa) ? $siswa->nama_ayah :  set_value('nama_ayah'); ?>">
																<?= form_error('nama_ayah', '<small class="text-danger">', '</small>'); ?>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label for="nama_ibu">Nama Ibu</label>
																<input type="text" class="form-control" name="nama_ibu" id="nama_ibu" placeholder="Nama Ibu" value="<?= isset($siswa) ? $siswa->nama_ibu :  set_value('nama_ibu'); ?>">
																<?= form_error('nama_ibu', '<small class="text-danger">', '</small>'); ?>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label for="berat">Berat Badan</label>
																<input type="text" class="form-control" name="berat" id="berat" placeholder="Berat Badan" value="<?= isset($siswa) ? $siswa->berat :  set_value('berat'); ?>">
																<?= form_error('berat', '<small class="text-danger">', '</small>'); ?>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label for="tinggi">Nama Ibu</label>
																<input type="text" class="form-control" name="tinggi" id="tinggi" placeholder="Tinggi" value="<?= isset($siswa) ? $siswa->tinggi :  set_value('tinggi'); ?>">
																<?= form_error('tinggi', '<small class="text-danger">', '</small>'); ?>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label for="golongan_darah">Berat Badan</label>
																<input type="text" class="form-control" name="golongan_darah" id="golongan_darah" placeholder="Golongan Darah" value="<?= isset($siswa) ? $siswa->golongan_darah :  set_value('golongan_darah'); ?>">
																<?= form_error('golongan_darah', '<small class="text-danger">', '</small>'); ?>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label for="riwayat_penyakit">Riwayat Penyakit</label>
																<input type="text" class="form-control" name="riwayat_penyakit" id="riwayat_penyakit" placeholder="Riwayat Penyakit" value="<?= isset($siswa) ? $siswa->riwayat_penyakit :  set_value('riwayat_penyakit'); ?>">
																<?= form_error('riwayat_penyakit', '<small class="text-danger">', '</small>'); ?>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label for="alamat">Alamat</label>
														<textarea class="form-control" name="alamat" id="alamat" rows="4"><?= isset($siswa) ? $siswa->alamat :  set_value('alamat'); ?></textarea>
														<?= form_error('alamat', '<small class="text-danger">', '</small>'); ?>
													</div>
													<button type="submit" class="btn btn-primary mr-2">Simpan</button>
													<a href="<?= base_url('Admin/Siswa') ?>" class="btn btn-light">Kembali</a>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade show" id="akun" role="tabpanel" aria-labelledby="akun-tab">
								<div class="row">
									<div class="col-lg-12 mt-4">
										<h3 class="text-dark">Akun Siswa</h3>
										<hr>
										<table class="text-dark" id="table_akun">
											<tr>
												<td>Username</td>
												<td>:</td>
												<td><?= $akun->username ?></td>
											</tr>
											<tr>
												<td>NISN</td>
												<td>:</td>
												<td><?= $akun->nisn ?></td>
											</tr>
											<tr>
												<td>No Telepon</td>
												<td>:</td>
												<td><?= $akun->no_hp ?></td>
											</tr>
											<tr>
												<td>Password</td>
												<td>:</td>
												<td><?= $akun->password ?></td>
											</tr>
											<tr>
												<td>Status</td>
												<td>:</td>
												<td class="status">
													<label class="badge <?= $akun->aktif == 1 ? 'badge-success' : 'badge-danger'; ?>"><?= $akun->aktif == 1 ? 'Aktif' : 'Belum Aktif' ?></label>
												</td>
											</tr>
											<tr>
												<?php if ($akun->aktif != 1) : ?>
													<td colspan="3">
														<button class="btn btn-sm btn-success btn-aktif" onclick="mengaktifkan(`<?= $akun->nisn; ?>`)">Aktifkan</button>
													</td>
												<?php endif; ?>
												<?php if ($akun->aktif == 1) : ?>
													<td colspan="3">
														<button class="btn btn-sm btn-danger btn-non-aktif" onclick="menonaktifkan(`<?= $akun->nisn; ?>`)">Non-Aktifkan</button>
													</td>
												<?php endif; ?>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<script src="<?= base_url('assets_me/js/admin/siswa_det.js'); ?>"></script>