<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
				<div class="col-12 grid-margin stretch-card">
					<div class="card">
						<div class="card-body">
							<h4 class="card-title"><?= $judul ?></h4>
							<!-- <p class="card-description"> Basic form elements </p> -->
							<form class="forms-sample" action="<?= base_url() ?>Admin/Jurusan/form" method="POST">
								<div class="form-group">
									<label for="jurusan">Jurusan</label>
									<input type="hidden" name="id" id="id" value="<?= isset($jurusan) ? $jurusan->id_jurusan : ''; ?>">
									<input type="text" value="<?= isset($jurusan) ? $jurusan->jurusan : ''; ?>" class="form-control" name="jurusan" id="jurusan" placeholder="Jurusan">
								</div>
								<div class="form-group">
									<label for="singkatan">Singkatan Jurusan</label>
									<input type="text" value="<?= isset($jurusan) ? $jurusan->singkatan : ''; ?>" class="form-control" name="singkatan" id="singkatan" placeholder="Singkatan Jurusan">
								</div>
								<button type="submit" class="btn btn-primary mr-2">Simpan</button>
								<a href="<?= base_url('Admin/Jurusan') ?>" class="btn btn-light">Kembali</a>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>