<div class="row">
	<div class="col-lg-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?= $judul ?></h4>
				<a href="<?= base_url('Admin/Kelas/Form') ?>" class="btn btn-primary mb-3">Tambah Kelas</a>
				<!-- <p class="card-description"> Add class <code>.table-bordered</code>
				</p> -->
				<table class="table table-bordered">
					<thead>
						<tr>
							<th> No. </th>
							<th> Kelas </th>
							<th> # </th>
						</tr>
					</thead>
					<tbody>
						<?php $no = 1 ?>
						<?php foreach ($kelas as $row) : ?>
							<tr>
								<td><?= $no++ ?></td>
								<td><?= $row->kelas ?></td>
								<td class="text-center">
									<div class="dropdown">
										<button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Options </button>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton5" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 39px, 0px);">
											<a class="dropdown-item" href="<?= base_url('Admin/Kelas/form/' . $row->id_kelas); ?>">Edit</a>
											<a class="dropdown-item" onclick="return confirm('Yakin untuk menghapus kelas?')" href="<?= base_url('Admin/Kelas/delete/' . $row->id_kelas); ?>">Hapus</a>
										</div>
									</div>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>