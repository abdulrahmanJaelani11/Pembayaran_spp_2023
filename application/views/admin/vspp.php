<div class="row">
	<div class="col-lg-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?= $judul ?></h4>
				<form action="<?= base_url('Admin/Pembayaran/ubah_tagihan'); ?>" method="post">
					<div class="form-group">
						<input type="text" name="tagihan" id="tagihan" class="form-control" style="border-top: 0; border-left: 0; border-right: 0;" placeholder="Masukan biaya SPP baru">
					</div>
					<div class="form-group">
						<input type="checkbox" name="all" id="all" value="1">
						<label for="all" class="text-dark">Untuk Semua Kelas</label>
					</div>
					<?php foreach ($kelas as $row) : ?>
						<div class="form-group">
							<input type="checkbox" name="<?= $row->kelas; ?>" id="<?= $row->kelas; ?>" value="1">
							<label for="<?= $row->kelas; ?>" class="text-dark">Kelas <?= $row->kelas; ?></label>
						</div>
					<?php endforeach; ?>
					<div class="form-group">
						<button type="submit" name="simpan" class="btn btn-sm btn-primary"> Simpan </button>
					</div>
				</form>
				cek
			</div>
		</div>
	</div>
</div>