<div class="row">
	<div class="col-lg-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?= $judul ?></h4>
				<?php if (isset($single_kelas)) : ?>
					<p class="text-dark">Kelas : <?= $single_kelas->kelas ?></p>
				<?php endif; ?>
				<div class="row">
					<div class="col-lg-2">
						<a href="<?= base_url('Admin/Siswa/Form') ?>" class="btn btn-primary mb-3">Tambah Siswa</a>
					</div>
					<!-- <div class="col-lg-10">
						<form action="<?= base_url('Admin/Siswa/import_excel') ?>" enctype="multipart/form-data">
							<div class="row justify-content-between">
								<div class="col-lg-8">
									<input type="file" name="userfile" id="userFile" class="form-control">
								</div>
								<div class="col-lg-4">
									<button class="btn btn-success" type="submit">Upload</button>
								</div>
							</div>
						</form>
					</div> -->
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" name="cari" id="cari" class="form-control" placeholder="Cari nama siswa">
						</div>
					</div>
					<div class="col-md-12">
						<div id="table"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	getTable('<?= isset($single_kelas) ? $single_kelas->id_kelas : ''; ?>')

	function getTable(kelas = null) {
		$.ajax({
			url: `<?= base_url('Admin/Siswa/getTable/') ?>` + kelas,
			success: function(data) {
				$("#table").html(data)
			}
		});
	}

	$("#cari").keyup(function() {
		$.ajax({
			url: `<?= base_url('Admin/Siswa/Cari'); ?>`,
			type: 'POST',
			data: {
				key: $(this).val(),
				id: `<?= $single_kelas->id_kelas; ?>`
			},
			success: function(data) {
				$('#table').html(data)
			}
		})
	})
</script>