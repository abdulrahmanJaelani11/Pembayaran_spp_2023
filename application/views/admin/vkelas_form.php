<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
				<div class="col-12 grid-margin stretch-card">
					<div class="card">
						<div class="card-body">
							<h4 class="card-title"><?= $judul ?></h4>
							<!-- <p class="card-description"> Basic form elements </p> -->
							<form class="forms-sample" action="<?= base_url() ?>Admin/Kelas/form" method="POST">
								<div class="form-group">
									<label for="kelas">Kelas</label>
									<input type="hidden" name="id" id="id" value="<?= isset($data_kelas) ? $data_kelas->id_kelas : ''; ?>">
									<input type="text" class="form-control" name="kelas" id="kelas" placeholder="Kelas" value="<?= isset($data_kelas) ? $data_kelas->kelas : ''; ?>">
								</div>
								<button type="submit" class="btn btn-primary mr-2">Simpan</button>
								<a href="<?= base_url('Admin/Kelas') ?>" class="btn btn-light">Kembali</a>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>