<div class="row">
	<div class="col-lg-7">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?= $judul; ?></h4>
				<p class="card-description"> Silahkan isi form dibawah dengan benar</p>
				<form class="forms-sample" action="<?= base_url('Admin/Pembayaran/form'); ?>" method="post">
					<div class="form-group">
						<label for="nisn">NISN</label>
						<input name="nisn" onchange="getSiswa(`<?= base_url('Admin/siswa/getSiswa'); ?>`, `<?= base_url('Admin/siswa/getSiswa_form'); ?>`)" type="text" class="form-control" id="nisn" placeholder="Masukan NISN siswa" autocomplete="off" value="<?= set_value('nisn'); ?>">
						<?= form_error('nisn', '<small class="text-danger">', '</small>'); ?>
					</div>
					<div class="form-group">
						<label for="nama">Nama Siswa</label>
						<input type="text" name="nama" class="form-control" id="nama" disabled>
					</div>
					<input type="hidden" name="id_siswa" class="form-control" id="id_siswa">
					<div class="form-group">
						<label for="kelas">Kelas</label>
						<input type="text" name="kelas" class="form-control" id="kelas" disabled>
					</div>
					<div class="form-group">
						<label for="pembayaran">Pembayaran</label>
						<select name="pembayaran" id="pembayaran" class="form-control" disabled>
						</select>
					</div>
					<div class="form-group">
						<label for="tagihan">Nominal Tagihan</label>
						<input type="text" name="tagihan" class="form-control" id="tagihan" placeholder="Masukan nisn untuk membayar" disabled>
					</div>
					<div class="form-group">
						<label for="terbayar">Nominal Terbayar</label>
						<input type="text" name="terbayar" class="form-control" id="terbayar" disabled>
					</div>
					<div class="form-group form_sisa_tagihan" style="display: none;">
						<label for="sisa_tagihan">Sisa Tagihan</label>
						<input type="text" class="form-control" id="sisa_tagihan" disabled>
					</div>
					<input type="hidden" name="sisa_tagihan">
					<div class="form-group">
						<label for="tanggal">Tanggal Pembayaran</label>
						<input type="date" class="form-control" name="tanggal" id="tanggal" placeholder="Masukan nominal pembayaran" value="<?= set_value('tanggal'); ?>">
						<?= form_error('tanggal', '<small class="text-danger">', '</small>'); ?>
					</div>
					<div class="form-group">
						<label for="nominal">Nominal Pembayaran</label>
						<input type="text" class="form-control" name="nominal" id="nominal" placeholder="Masukan nominal pembayaran" value="<?= set_value('nominal'); ?>">
						<?= form_error('nominal', '<small class="text-danger">', '</small>'); ?>
					</div>
					<button type="submit" onclick="return validasi()" class="btn btn-primary mr-2">Simpan</button>
					<a href="<?= base_url('Admin/Pembayaran'); ?>" class="btn btn-light">Batal</a>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-5 info_detail" style="display: none;">
		<div class="row">
			<div class="col-md-12 mb-3">
				<div class="card text-dark">
					<div class="card-body detail">

					</div>
				</div>
			</div>
			<div class="col-md-12 mb-3">
				<div class="card text-dark">
					<div class="card-body detail_pembayaran">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>