<div class="container-scroller">
	<div class="container-fluid page-body-wrapper full-page-wrapper">
		<div class="content-wrapper d-flex align-items-center auth">
			<div class="row flex-grow">
				<div class="col-lg-4 mx-auto">
					<div class="auth-form-light text-left p-5">
						<div class="brand-logo">
							<img src="<?= base_url(); ?>assets_me/images/logo-dark.svg">
						</div>
						<h4>Selamat Datang</h4>
						<h6 class="font-weight-light">Silahkan login untuk melanjutkan</h6>

						<!-- ALERT -->
						<?php if ($this->session->flashdata('sukses')) : ?>
							<div class="alert alert-success">
								<?= $_SESSION['sukses']; ?>
							</div>
						<?php endif ?>
						<?php if ($this->session->flashdata('error')) : ?>
							<div class="alert alert-danger">
								<?= $_SESSION['error']; ?>
							</div>
						<?php endif ?>

						<form class="pt-3" action="<?= base_url('Admin/Auth'); ?>" method="POST">
							<div class="form-group">
								<input type="email" class="form-control form-control-lg" name="email" id="email" value="<?= set_value('email'); ?>" placeholder="Email">
								<?= form_error('email', '<small class="text-danger">', '</small>') ?>
							</div>
							<div class="form-group">
								<input type="password" class="form-control form-control-lg" name="password" id="password" placeholder="Password">
								<?= form_error('password', '<small class="text-danger">', '</small>') ?>
							</div>
							<div class="mt-3">
								<button type="submit" name="login" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn"> Login </button>
							</div>
							<div class="my-2 d-flex justify-content-between align-items-center">
								<a href="<?= base_url('Admin/Auth/lupa_password'); ?>" class="auth-link text-black">Lupa Password?</a>
								<a href="<?= base_url('Siswa/Auth'); ?>" class="auth-link text-black">Login Siswa</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- content-wrapper ends -->
	</div>
	<!-- page-body-wrapper ends -->
</div>