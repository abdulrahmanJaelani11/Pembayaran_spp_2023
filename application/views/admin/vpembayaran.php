<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title"><?= $judul ?> Terbaru di bulan <?= date('M'); ?></h4>
                <!-- <p class="card-description"> Add class <code>.table-bordered</code>
				</p> -->
                <table class="table table-bordered " id="table">
                    <thead>
                        <tr>
                            <th> No. </th>
                            <th> Nama </th>
                            <th> NISN </th>
                            <th> Kelas </th>
                            <th> Jurusan </th>
                            <th> Tgl Bayar </th>
                            <th> Tahun </th>
                            <th> Bulan </th>
                            <th> Tagihan </th>
                            <th> Terbayar </th>
                            <th> Status </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1 ?>
                        <?php foreach ($pembayaran as $row) : ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><?= $row->nama ?></td>
                                <td><?= $row->nisn ?></td>
                                <td><?= $row->kelas ?></td>
                                <td><?= $row->singkatan ?></td>
                                <td><?= $row->tgl_bayar ?></td>
                                <td><?= $row->tahun ?></td>
                                <td><?= $row->bulan ?></td>
                                <td>Rp.<?= number_format($row->tagihan, 2, ',', ',') ?></td>
                                <td>Rp.<?= number_format($row->terbayar, 2, ',', ',') ?></td>
                                <td>
                                    <label class="badge <?= $row->status === 'paid' ? 'badge-success' : 'badge-danger'; ?>"><?= $row->status === 'paid' ? 'Lunas' : 'Belum Lunas' ?></label>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table').DataTable();
    });
</script>