<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
            <div class="row flex-grow">
                <div class="col-lg-4 mx-auto">
                    <div class="auth-form-light text-left p-5">
                        <div class="brand-logo">
                            <img src="<?= base_url(); ?>assets_me/images/logo-dark.svg">
                        </div>
                        <h4>Selamat Datang</h4>
                        <h6 class="font-weight-light">Silahkan masukan email untuk mereset password</h6>
                        <form class="pt-3">
                            <div class="form-group">
                                <input type="email" class="form-control form-control-lg" name="email" id="email" placeholder="Email">
                            </div>
                            <div class="mt-3">
                                <button type="submit" name="login" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn"> Login </button>
                            </div>
                            <div class="my-2 d-flex justify-content-between align-items-center">
                                <a href="" class="auth-link text-black">Lupa Password?</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>