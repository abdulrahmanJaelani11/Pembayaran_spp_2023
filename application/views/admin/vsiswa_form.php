<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card text-dark">
			<div class="card-body">
				<h4 class="card-title"><?= $judul ?></h4>
				<!-- <p class="card-description"> Basic form elements </p> -->
				<form class="forms-sample" action="<?= base_url() ?>Admin/Siswa/form" method="POST">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="nama">Nama</label>
								<input type="hidden" name="id" id="id" value="<?= isset($student) ? $student->id : ''; ?>">
								<input type="text" class="form-control" value="<?= isset($student) ? $student->nama :  set_value('nama'); ?>" name="nama" id="nama" placeholder="Nama">
								<?= form_error('nama', '<small class="text-danger">', '</small>'); ?>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="nisn">NISN</label>
								<input type="text" class="form-control" name="nisn" id="nisn" placeholder="NISN" value="<?= isset($student) ? $student->nisn :  set_value('nisn'); ?>">
								<?= form_error('nisn', '<small class="text-danger">', '</small>'); ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="jenis_kelamin">Jenis Kelamin</label>
								<select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
									<option value="">- Pilih Jenis Kelamin -</option>
									<option value="Laki-laki" <?= isset($student) ? ($student->jenis_kelamin == 'Laki-laki' ? 'selected' : '') : ''; ?>>Laki-laki</option>
									<option value="Perempuan" <?= isset($student) ? ($student->jenis_kelamin == 'Perempuan' ? 'selected' : '') : ''; ?>>Perempuan</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="kelas">Kelas</label>
								<select class="form-control" name="kelas" id="kelas">
									<option value="">-- Pilih Kelas --</option>
									<?php foreach ($kelas as $row) : ?>
										<option <?= isset($student) ? ($student->id_kelas == $row->id_kelas ? 'selected' : '')  : ''; ?> value="<?= $row->id_kelas ?>"><?= $row->kelas ?></option>
									<?php endforeach ?>
								</select>
								<?= form_error('kelas', '<small class="text-danger">', '</small>'); ?>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="jurusan">Jurusan</label>
								<select class="form-control" name="jurusan" id="jurusan">
									<option value="">-- Pilih Jurusan --</option>
									<?php foreach ($jurusan as $row) : ?>
										<option <?= isset($student) ? ($student->id_jurusan == $row->id_jurusan ? 'selected' : '')  : ''; ?> value="<?= $row->id_jurusan ?>"><?= $row->jurusan ?></option>
									<?php endforeach ?>
								</select>
								<?= form_error('jurusan', '<small class="text-danger">', '</small>'); ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="no_hp">No HP</label>
								<input type="number" class="form-control" name="no_hp" id="no_hp" placeholder="No. HP" value="<?= isset($student) ? $student->no_hp :  set_value('no_hp'); ?>">
								<?= form_error('no_hp', '<small class="text-danger">', '</small>'); ?>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="tanggal_lahir">Tanggal Lahir</label>
								<input type="date" class="form-control" name="tanggal_lahir" id="tanggal_lahir" placeholder="Tanggal Lahir" value="<?= isset($student) ? $student->tanggal_lahir :  set_value('tanggal_lahir'); ?>">
								<?= form_error('tanggal_lahir', '<small class="text-danger">', '</small>'); ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="tempat_lahir">Tempat Lahir</label>
								<input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir" value="<?= isset($student) ? $student->tempat_lahir :  set_value('tempat_lahir'); ?>">
								<?= form_error('tempat_lahir', '<small class="text-danger">', '</small>'); ?>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="agama">Agama</label>
								<select name="agama" id="agama" class="form-control">
									<option value="">- Pilih agama -</option>
									<option value="islam" <?= isset($student) ? ($student->agama == 'islam' ? 'selected' : '') : ''; ?>>Islam</option>
									<option value="katholik" <?= isset($student) ? ($student->agama == 'katholik' ? 'selected' : '') : ''; ?>>Katholik</option>
									<option value="hindu" <?= isset($student) ? ($student->agama == 'hindu' ? 'selected' : '') : ''; ?>>Hindu</option>
									<option value="budha" <?= isset($student) ? ($student->agama == 'budha' ? 'selected' : '') : ''; ?>>Budha</option>
									<option value="konghucu" <?= isset($student) ? ($student->agama == 'konghucu' ? 'selected' : '') : ''; ?>>Konghucu</option>
									<option value="kristen" <?= isset($student) ? ($student->agama == 'kristen' ? 'selected' : '') : ''; ?>>Kristen</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="nama_ayah">Nama Ayah</label>
								<input type="text" class="form-control" name="nama_ayah" id="nama_ayah" placeholder="Nama Ayah" value="<?= isset($student) ? $student->nama_ayah :  set_value('nama_ayah'); ?>">
								<?= form_error('nama_ayah', '<small class="text-danger">', '</small>'); ?>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="nama_ibu">Nama Ibu</label>
								<input type="text" class="form-control" name="nama_ibu" id="nama_ibu" placeholder="Nama Ibu" value="<?= isset($student) ? $student->nama_ibu :  set_value('nama_ibu'); ?>">
								<?= form_error('nama_ibu', '<small class="text-danger">', '</small>'); ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="berat">Berat Badan</label>
								<input type="text" class="form-control" name="berat" id="berat" placeholder="Berat Badan" value="<?= isset($student) ? $student->berat :  set_value('berat'); ?>">
								<?= form_error('berat', '<small class="text-danger">', '</small>'); ?>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="tinggi">Nama Ibu</label>
								<input type="text" class="form-control" name="tinggi" id="tinggi" placeholder="Tinggi" value="<?= isset($student) ? $student->tinggi :  set_value('tinggi'); ?>">
								<?= form_error('tinggi', '<small class="text-danger">', '</small>'); ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="golongan_darah">Berat Badan</label>
								<input type="text" class="form-control" name="golongan_darah" id="golongan_darah" placeholder="Golongan Darah" value="<?= isset($student) ? $student->golongan_darah :  set_value('golongan_darah'); ?>">
								<?= form_error('golongan_darah', '<small class="text-danger">', '</small>'); ?>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="riwayat_penyakit">Riwayat Penyakit</label>
								<input type="text" class="form-control" name="riwayat_penyakit" id="riwayat_penyakit" placeholder="Riwayat Penyakit" value="<?= isset($student) ? $student->riwayat_penyakit :  set_value('riwayat_penyakit'); ?>">
								<?= form_error('riwayat_penyakit', '<small class="text-danger">', '</small>'); ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="alamat">Alamat</label>
						<textarea class="form-control" name="alamat" id="alamat" rows="4"><?= isset($student) ? $student->alamat :  set_value('alamat'); ?></textarea>
						<?= form_error('alamat', '<small class="text-danger">', '</small>'); ?>
					</div>
					<button type="submit" class="btn btn-primary mr-2">Simpan</button>
					<a href="<?= base_url('Admin/Siswa') ?>" class="btn btn-light">Kembali</a>
				</form>
			</div>
		</div>
	</div>
</div>