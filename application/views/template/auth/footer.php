<script src="<?= base_url(); ?>assets_me/js/sweetalert2.js"></script>
<?php if ($this->session->flashdata('sukses')) : ?>
    <script>
        Swal.fire(
            'Sukses',
            `<?= $_SESSION['sukses']; ?>`,
            'success'
        )
    </script>
<?php endif ?>
<?php if ($this->session->flashdata('error')) : ?>
    <script>
        Swal.fire(
            'Oppss',
            `<?= $_SESSION['error']; ?>`,
            'error'
        )
    </script>
<?php endif ?>
<!-- container-scroller -->
<!-- plugins:js -->
<script src="<?= base_url(); ?>assets_me/vendors/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page -->
<!-- End plugin js for this page -->
<!-- inject:js -->
<script src="<?= base_url(); ?>assets_me/js/off-canvas.js"></script>
<script src="<?= base_url(); ?>assets_me/js/hoverable-collapse.js"></script>
<script src="<?= base_url(); ?>assets_me/js/misc.js"></script>
<!-- endinject -->
</body>

</html>