<nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
	<div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
		<a class="navbar-brand brand-logo" href="<?= base_url('Admin/Dashboard') ?>"><img src="<?= base_url() ?>assets_me/images/logo.svg" alt="logo" /></a>
		<a class="navbar-brand brand-logo-mini" href="<?= base_url() ?>"><img src="<?= base_url() ?>assets_me/images/logo-mini.svg" alt="logo" /></a>
	</div>
	<div class="navbar-menu-wrapper d-flex align-items-stretch">
		<button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
			<span class="mdi mdi-menu"></span>
		</button>
		<ul class="navbar-nav navbar-nav-right">
			<li class="nav-item nav-profile dropdown">
				<a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
					<div class="nav-profile-img">
						<img src="<?= base_url() ?>assets_me/images/profile/default.png" alt="image">
					</div>
					<div class="nav-profile-text">
						<p class="mb-1 text-black"><?= $siswa->nama; ?></p>
					</div>
				</a>
				<div class="dropdown-menu navbar-dropdown dropdown-menu-right p-0 border-0 font-size-sm" aria-labelledby="profileDropdown" data-x-placement="bottom-end">
					<div class="p-3 text-center bg-primary">
						<img class="img-avatar img-avatar48 img-avatar-thumb" src="<?= base_url() ?>assets_me/images/profile/default.png" alt="">
					</div>
					<div class="p-2">
						<h5 class="dropdown-header text-uppercase pl-2 text-dark">User Options</h5>
						<a class="dropdown-item py-1 d-flex align-items-center justify-content-between" href="<?= base_url('Siswa/Siswa/profil'); ?>">
							<span>Profil</span>
							<span class="p-0">
								<span class="badge badge-success">1</span>
								<i class="mdi mdi-account-outline ml-1"></i>
							</span>
						</a>
						<div role="separator" class="dropdown-divider"></div>
						<h5 class="dropdown-header text-uppercase  pl-2 text-dark mt-2">Actions</h5>
						<a class="dropdown-item py-1 d-flex align-items-center justify-content-between" href="<?= base_url('Siswa/Auth/logout'); ?>">
							<span>Log Out</span>
							<i class="mdi mdi-logout ml-1"></i>
						</a>
					</div>
				</div>
			</li>
		</ul>
		<button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
			<span class="mdi mdi-menu"></span>
		</button>
	</div>
</nav>

<!-- partial -->
<div class="container-fluid page-body-wrapper">
	<nav class="sidebar sidebar-offcanvas" id="sidebar">
		<ul class="nav">
			<li class="nav-item nav-category">Main</li>
			<li class="nav-item">
				<a class="nav-link" href="<?= base_url('Siswa/Dashboard') ?>">
					<span class="icon-bg"><i class="mdi mdi-cube menu-icon"></i></span>
					<span class="menu-title">Dashboard</span>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?= base_url('Siswa/Pembayaran/index') ?>">
					<span class="icon-bg"><i class="mdi mdi-cube menu-icon"></i></span>
					<span class="menu-title">Pembayaran</span>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?= base_url('Siswa/Pembayaran/history') ?>">
					<span class="icon-bg"><i class="mdi mdi-cube menu-icon"></i></span>
					<span class="menu-title">Riwayat Pembayaran</span>
				</a>
			</li>
			<!-- <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                    <span class="icon-bg"><i class="mdi mdi-crosshairs-gps menu-icon"></i></span>
                    <span class="menu-title"></span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="ui-basic">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item"> <a class="nav-link" href="<?= base_url() ?>">My Profile</a></li>
                    </ul>
                </div>
            </li> -->
			<!-- </li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
					<span class="icon-bg"><i class="mdi mdi-lock menu-icon"></i></span>
					<span class="menu-title">Pembayaran</span>
					<i class="menu-arrow"></i>
				</a>
				<div class="collapse" id="auth">
					<ul class="nav flex-column sub-menu">
						<li class="nav-item"> <a class="nav-link" href="<?= base_url() ?>/pages/samples/blank-page.html"> Daftar Pembayaran </a></li>
					</ul>
				</div>
			</li> -->
			<!-- <li class="nav-item">
				<a class="nav-link" href="<?= base_url() ?>/pages/icons/mdi.html">
					<span class="icon-bg"><i class="mdi mdi-contacts menu-icon"></i></span>
					<span class="menu-title">Icons</span>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?= base_url() ?>/pages/forms/basic_elements.html">
					<span class="icon-bg"><i class="mdi mdi-format-list-bulleted menu-icon"></i></span>
					<span class="menu-title">Forms</span>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?= base_url() ?>/pages/charts/chartjs.html">
					<span class="icon-bg"><i class="mdi mdi-chart-bar menu-icon"></i></span>
					<span class="menu-title">Charts</span>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?= base_url() ?>/pages/tables/basic-table.html">
					<span class="icon-bg"><i class="mdi mdi-table-large menu-icon"></i></span>
					<span class="menu-title">Tables</span>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
					<span class="icon-bg"><i class="mdi mdi-lock menu-icon"></i></span>
					<span class="menu-title">User Pages</span>
					<i class="menu-arrow"></i>
				</a>
				<div class="collapse" id="auth">
					<ul class="nav flex-column sub-menu">
						<li class="nav-item"> <a class="nav-link" href="<?= base_url() ?>/pages/samples/blank-page.html"> Blank Page </a></li>
						<li class="nav-item"> <a class="nav-link" href="<?= base_url() ?>/pages/samples/login.html"> Login </a></li>
						<li class="nav-item"> <a class="nav-link" href="<?= base_url() ?>/pages/samples/register.html"> Register </a></li>
						<li class="nav-item"> <a class="nav-link" href="<?= base_url() ?>/pages/samples/error-404.html"> 404 </a></li>
						<li class="nav-item"> <a class="nav-link" href="<?= base_url() ?>/pages/samples/error-500.html"> 500 </a></li>
					</ul>
				</div>
			</li>
			<li class="nav-item documentation-link">
				<a class="nav-link" href="http://www.bootstrapdash.com/demo/connect-plus-free/jquery/documentation/documentation.html" target="_blank">
					<span class="icon-bg">
						<i class="mdi mdi-file-document-box menu-icon"></i>
					</span>
					<span class="menu-title">Documentation</span>
				</a>
			</li>
			<li class="nav-item sidebar-user-actions">
				<div class="user-details">
					<div class="d-flex justify-content-between align-items-center">
						<div>
							<div class="d-flex align-items-center">
								<div class="sidebar-profile-img">
									<img src="<?= base_url() ?>/assets_me/images/faces/face28.png" alt="image">
								</div>
								<div class="sidebar-profile-text">
									<p class="mb-1">Henry Klein</p>
								</div>
							</div>
						</div>
						<div class="badge badge-danger">3</div>
					</div>
				</div>
			</li>
			<li class="nav-item sidebar-user-actions">
				<div class="sidebar-user-menu">
					<a href="#" class="nav-link"><i class="mdi mdi-settings menu-icon"></i>
						<span class="menu-title">Settings</span>
					</a>
				</div>
			</li>
			<li class="nav-item sidebar-user-actions">
				<div class="sidebar-user-menu">
					<a href="#" class="nav-link"><i class="mdi mdi-speedometer menu-icon"></i>
						<span class="menu-title">Take Tour</span></a>
				</div>
			</li>
			<li class="nav-item sidebar-user-actions">
				<div class="sidebar-user-menu">
					<a href="#" class="nav-link"><i class="mdi mdi-logout menu-icon"></i>
						<span class="menu-title">Log Out</span></a>
				</div>
			</li> -->
		</ul>
	</nav>
	<!-- partial -->
	<div class="main-panel">
		<div class="content-wrapper">