<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Pembayaran SPP</title>
	<!-- plugins:css -->
	<link rel="stylesheet" href="<?= base_url() ?>/assets_me/vendors/mdi/css/materialdesignicons.min.css">
	<link rel="stylesheet" href="<?= base_url() ?>/assets_me/vendors/flag-icon-css/css/flag-icon.min.css">
	<link rel="stylesheet" href="<?= base_url() ?>/assets_me/vendors/css/vendor.bundle.base.css">
	<link rel="stylesheet" href="<?= base_url() ?>/assets_me/css/datatables.min.css" />

	<!-- endinject -->
	<!-- Plugin css for this page -->
	<!-- End plugin css for this page -->
	<!-- inject:css -->
	<!-- endinject -->
	<!-- Layout styles -->
	<link rel="stylesheet" href="<?= base_url() ?>/assets_me/css/style.css">
	<link rel="stylesheet" href="<?= base_url() ?>/assets_me/css/admin/siswa_det.css">
	<!-- End layout styles -->
	<script src="<?= base_url() ?>/assets_me/js/jquery.js"></script>
	<script src="<?= base_url() ?>/assets_me/js/datatables.js"></script>
	<script src="<?= base_url() ?>/assets_me/js/datatables.min.js"></script>
	<link rel="shortcut icon" href="<?= base_url() ?>/assets_me/images/favicon.png" />
	<script>
		let base_url = `<?= base_url(); ?>`
	</script>
</head>

<body>
	<div class="container-scroller">