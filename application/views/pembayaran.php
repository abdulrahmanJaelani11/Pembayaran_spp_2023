<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Bootstrap demo</title>
	<link rel="stylesheet" href="<?= base_url('assets_me/css/bootstrap.min.css') ?>">

	<script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="SB-Mid-client-DP8SMsONFunaoJn0"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
</head>

<body>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-6">
				<div class="card mt-5">
					<div class="card-body">
						<h3>Form Pembayaran</h3>
						<form action=""></form>
						<form id="payment-form" method="post" action="<?= site_url() ?>/snap/finish">
							<input type="hidden" name="result_type" id="result-type" value="">
							<input type="hidden" name="result_data" id="result-data" value="">
							<div class="form-group mb-3">
								<label for="nama">Nama</label>
								<input type="text" class="form-control" name="nama" id="nama" placeholder="Nama">
							</div>
							<div class="form-group mb-3">
								<label for="kelas">Kelas</label>
								<input type="text" class="form-control" name="kelas" id="kelas" placeholder="Kelas">
							</div>
							<div class="form-group mb-3">
								<label for="number">Nominal</label>
								<input type="number" class="form-control" name="nominal" id="nominal" placeholder="Nominal">
							</div>
							<button id="pay-button" class="btn btn-primary">Bayar!</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$('#pay-button').click(function(event) {
			event.preventDefault();
			$(this).attr("disabled", "disabled");

			let nama = $("#nama").val()
			let kelas = $("#kelas").val()
			let nominal = $("#nominal").val()
			$.ajax({
				type: 'POST',
				url: '<?= site_url() ?>/snap/token',
				data: {
					nama: nama,
					kelas: kelas,
					nominal: nominal
				},
				cache: false,

				success: function(data) {
					//location = data;

					console.log('token = ' + data);

					var resultType = document.getElementById('result-type');
					var resultData = document.getElementById('result-data');

					function changeResult(type, data) {
						$("#result-type").val(type);
						$("#result-data").val(JSON.stringify(data));
						//resultType.innerHTML = type;
						//resultData.innerHTML = JSON.stringify(data);
					}

					snap.pay(data, {

						onSuccess: function(result) {
							changeResult('success', result);
							console.log(result.status_message);
							console.log(result);
							$("#payment-form").submit();
						},
						onPending: function(result) {
							changeResult('pending', result);
							console.log(result.status_message);
							$("#payment-form").submit();
						},
						onError: function(result) {
							changeResult('error', result);
							console.log(result.status_message);
							$("#payment-form").submit();
						}
					});
				}
			});
		});
	</script>
</body>

</html>