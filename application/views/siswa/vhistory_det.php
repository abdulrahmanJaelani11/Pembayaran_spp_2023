<style>
	table .semicolon {
		padding: 0 5px;
	}

	#table_pem td {
		padding: 4px;
	}

	.bank_area {
		height: 210px;
		border-left: 1px solid #33C92D;
	}

	@media screen and (max-width: 400px) {
		table {
			font-size: 13px;
		}

		.bank_area {
			font-size: 13px;
			padding-top: 15px;
			border-left: 0 solid white;
			/* background-color: #33C92D; */
		}
	}
</style>
<div class="row">
	<div class="col-12">
		<div class="card text-dark">
			<div class="card-body">
				<div class="row">
					<div class="col-md-4" style="border-radius: 20px; border: 1px solid; border-color: #33C92D;">
						<div class="p-2">
							<table>
								<tr>
									<td>Nama</td>
									<td class="semicolon">:</td>
									<td><?= $pembayaran[0]->nama; ?></td>
								</tr>
								<tr>
									<td>NISN</td>
									<td class="semicolon">:</td>
									<td><?= $pembayaran[0]->nisn; ?></td>
								</tr>
								<tr>
									<td>Kelas</td>
									<td class="semicolon">:</td>
									<td><?= $pembayaran[0]->kelas; ?></td>
								</tr>
								<tr>
									<td>Jurusan</td>
									<td class="semicolon">:</td>
									<td><?= $pembayaran[0]->jurusan; ?></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-4">
						<table id="table_pem">
							<tr>
								<td>Pembayaran ID</td>
								<td class="semicolon">:</td>
								<td><?= $pembayaran[0]->order_id; ?></td>
							</tr>
							<tr>
								<td>Pembayaran Tahun</td>
								<td class="semicolon">:</td>
								<td><?= $pembayaran[0]->tahun; ?></td>
							</tr>
							<tr>
								<td>Pembayaran Bulan</td>
								<td class="semicolon">:</td>
								<td><?= $pembayaran[0]->bulan; ?></td>
							</tr>
							<tr>
								<td>Metode Pembayaran</td>
								<td class="semicolon">:</td>
								<td><?= $pembayaran[0]->metode_pembayaran; ?></td>
							</tr>
							<tr>
								<td>Nominal Tagihan</td>
								<td class="semicolon">:</td>
								<td>Rp. <?= number_format($pembayaran[0]->tagihan, 0, ',', '.'); ?></td>
							</tr>
							<tr>
								<td>Nominal Terbayar</td>
								<td class="semicolon">:</td>
								<td>Rp. <?= $pembayaran[0]->terbayar != '' ? number_format($pembayaran[0]->terbayar, 0, ',', '.') : '0' ?></td>
							</tr>
							<tr>
								<td>Status Pembayaran</td>
								<td class="semicolon">:</td>
								<td>
									<label class="badge <?= $pembayaran[0]->status === 'paid' ? 'badge-success' : 'badge-danger'; ?>"><?= $pembayaran[0]->status === 'paid' ? 'Lunas' : 'Belum Lunas' ?></label>
								</td>
							</tr>
							<tr>
								<td>Tanggal Pembayaran</td>
								<td class="semicolon">:</td>
								<td><?= $pembayaran[0]->tgl_bayar; ?></td>
							</tr>
						</table>
					</div>
					<div class="col-md-6 bank_area">
						<!-- <div class="garis" style="height: 100px; background-color: #33C92D; width: 0;"></div> -->
						<?php if ($pembayaran[0]->metode_pembayaran == 'cash') : ?>
							<img src="<?= base_url('assets_me/images/bank/' . $pembayaran[0]->metode_pembayaran . '.png') ?>" alt="BANK" style="width: 100px;"><br>
							<p>Pembayaran secara cash diberikan kepada petugas/guru di sekolah</p>
						<?php else : ?>
							<img src="<?= base_url('assets_me/images/bank/' . $pembayaran[0]->bank . '.png') ?>" alt="BANK" style="width: 100px;"><br>
							<div class="font-weight-bold pt-2">Kode Pembayaran - <?= strtoupper($pembayaran[0]->bank) ?></div>
							<div style="font-size: 25px; margin-top: 6px;"><?= $pembayaran[0]->kode_pembayaran ?></div>
							<p>Silahkan lakukan pembayaran ke NO. virtual akun diatas dalam 1 jam</p>
						<?php endif ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>