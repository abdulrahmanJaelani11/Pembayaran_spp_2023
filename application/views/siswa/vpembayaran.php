<style>
	#table_pem td {
		padding: 5px;
	}

	@media screen and (max-width: 400px) {
		#table_pem td {
			font-size: 13px;
			/* padding: 10px; */
		}

		#table_pem button {
			font-size: 10px;
		}

		#table_pem .semicolon {
			padding: 0 2px;
			margin: 0;
		}
	}
</style>
<div class="row">
	<div class="col-md-12">
		<div class="d-sm-flex justify-content-between align-items-center transaparent-tab-border {">
			<ul class="nav nav-tabs tab-transparent" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="siswa-det-tab" data-toggle="tab" href="#siswa-detail" role="tab" aria-selected="true">Pembayaran Bulan Ini</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="business-tab" data-toggle="tab" href="#business-1" role="tab" aria-selected="false">Pembayaran Terbayar</a>
				</li>
			</ul>
			<div class="d-md-block d-none">
				<a href="#" class="text-light p-1"><i class="mdi mdi-view-dashboard"></i></a>
				<a href="#" class="text-light p-1"><i class="mdi mdi-dots-vertical"></i></a>
			</div>
		</div>
		<div class="tab-content tab-transparent-content">
			<div class="tab-pane fade show active" id="siswa-detail" role="tabpanel" aria-labelledby="siswa-det-tab">
				<div class="row">
					<div class="col-lg-6">
						<div class="card text-dark">
							<!-- <div class="card-body">
								<h4>Pembayaran SPP Bulan ini</h4>
								<hr>
								<table width="100%" id="table_pem">
									<tr>
										<td>Nama</td>
										<td class="semicolon">:</td>
										<td><?= $pembayaran_bulan_ini->nama; ?></td>
									</tr>
									<tr>
										<td>NISN</td>
										<td class="semicolon">:</td>
										<td><?= $pembayaran_bulan_ini->nisn; ?></td>
									</tr>
									<tr>
										<td>Kelas</td>
										<td class="semicolon">:</td>
										<td><?= $pembayaran_bulan_ini->kelas; ?></td>
									</tr>
									<tr>
										<td>Jurusan</td>
										<td class="semicolon">:</td>
										<td><?= $pembayaran_bulan_ini->jurusan; ?></td>
									</tr>
									<tr>
										<td>Tahun</td>
										<td class="semicolon">:</td>
										<td><?= $pembayaran_bulan_ini->tahun; ?></td>
									</tr>
									<tr>
										<td>Bulan</td>
										<td class="semicolon">:</td>
										<td><?= $pembayaran_bulan_ini->bulan; ?></td>
									</tr>
									<tr>
										<td>Tagihan SPP</td>
										<td class="semicolon">:</td>
										<td>Rp. <?= number_format($pembayaran_bulan_ini->tagihan, 0, '.', '.'); ?></td>
									</tr>
									<tr>
										<td>Terbayar</td>
										<td class="semicolon">:</td>
										<td><?= ($pembayaran_bulan_ini->terbayar != 0) ? 'RP. ' . number_format($pembayaran_bulan_ini->terbayar, 0, '.', ',') : 0; ?></td>
									</tr>
									<tr>
										<td>Status</td>
										<td class="semicolon">:</td>
										<td><label class="badge <?= $pembayaran_bulan_ini->status === 'paid' ? 'badge-success' : 'badge-danger'; ?>"><?= $pembayaran_bulan_ini->status === 'paid' ? 'Lunas' : 'Belum Lunas' ?></label></td>
									</tr>
									<tr>
										<td colspan="3">
											<button type="button" <?= $pembayaran_bulan_ini->status === 'paid' ? 'disabled' : ''; ?> id="pay-button" name="bayar" class="btn btn-primary" style="width: 100%;"> Bayar </button>
										</td>
									</tr>
								</table>
								<form action="<?= base_url('Siswa/Pembayaran/addPembayaran'); ?>" id="payment-form" method="post">
									<input type="hidden" name="id" id="id" value="<?= $pembayaran_bulan_ini->id_pembayaran; ?>">
									<input type="hidden" name="tagihan" id="tagihan" value="<?= $pembayaran_bulan_ini->tagihan; ?>">
									<input type="hidden" name="orderid" id="orderid">
									<input type="hidden" name="transaksi_id" id="transaksi_id">
									<input type="hidden" name="kode_pembayaran" id="kode_pembayaran">
									<input type="hidden" name="mandiri_kode" id="mandiri_kode">
									<input type="hidden" name="bank" id="bank">
									<input type="hidden" name="metode_pembayaran" id="metode_pembayaran">
									<input type="hidden" name="tanggal_transaksi" id="tanggal_transaksi">
									<input type="hidden" name="status" id="status">
								</form>
							</div> -->
							<div class="card-body">
								<h5>Form Pembayaran</h5>
								<form action="<?= base_url('Siswa/Pembayaran/addPembayaran'); ?>" id="payment_form" method="POST">
									<div class="form-group">
										<input type="text" name="nama" id="nama" class="form-control" style="border-top: 0; border-left: 0; border-right: 0; background-color: white;" placeholder="Nama" value="<?= $siswa->nama ?>" disabled>
									</div>
									<div class="form-group">
										<input type="text" name="nisn" id="nisn" class="form-control" style="border-top: 0; border-left: 0; border-right: 0; background-color: white;" placeholder="NISN" value="<?= $siswa->nisn ?>" disabled>
									</div>
									<div class="form-group">
										<input type="text" name="kelas" id="kelas" class="form-control" style="border-top: 0; border-left: 0; border-right: 0; background-color: white;" placeholder="Kelas" value="<?= $siswa->kelas ?>" disabled>
									</div>
									<div class="form-group">
										<input type="text" name="jurusan" id="jurusan" class="form-control" style="border-top: 0; border-left: 0; border-right: 0; background-color: white;" placeholder="Jurusan" value="<?= $siswa->jurusan ?>" disabled>
									</div>
									<div class="form-group">
										<select name="byr_bulan" id="byr_bulan" class="form-control" style="border-top: 0; border-left: 0; border-right: 0;">
											<?php foreach ($belum_terbayar as $row) : ?>
												<option <?= $row->index_bulan == date('m') ? 'selected' : '' ?> value="<?= $row->id_pembayaran ?>"><?= $row->bulan ?></option>
											<?php endforeach ?>
										</select>
										<input type="hidden" name="bulan" id="bulan" value="<?= $row->bulan . ' ' . date('Y') ?>">
									</div>
									<div class="form-group">
										<input type="text" id="tagihan" class="form-control" style="border-top: 0; border-left: 0; border-right: 0; background-color: white;" placeholder="Nominal Tagihan" value="<?= $pembayaran_bulan_ini->tagihan ?>" disabled>
										<input type="hidden" name="tagihan" value="<?= $pembayaran_bulan_ini->tagihan ?>">
									</div>

									<input type="hidden" name="id" id="id" value="<?= $pembayaran_bulan_ini->id_pembayaran ?>">
									<input type="hidden" name="orderid" id="orderid">
									<input type="hidden" name="transaksi_id" id="transaksi_id">
									<input type="hidden" name="kode_pembayaran" id="kode_pembayaran">
									<input type="hidden" name="mandiri_kode" id="mandiri_kode">
									<input type="hidden" name="bank" id="bank">
									<input type="hidden" name="metode_pembayaran" id="metode_pembayaran">
									<input type="hidden" name="tanggal_transaksi" id="tanggal_transaksi">
									<input type="hidden" name="status" id="status">

									<!-- <button type="submit" class="payment_button"></button> -->
									<div class="form-group">
										<button type="button" id="pay-button" class="btn btn-success" style="width: 100%;">Konfirmasi Pembayaran</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane fade show" id="business-1" role="tabpanel" aria-labelledby="business-tab">
				<div class="row">
					<div class="col-lg-12 mt-3">
						<div class="card">
							<div class="card-body">
								<h4>Sudah Terbayar</h4>
								<table class="table table-bordered table-responsive">
									<thead>
										<tr>
											<th>Tahun</th>
											<th>Bulan</th>
											<th>Tanggal</th>
											<th>Terbayar</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($terbayar as $row) : ?>
											<tr>
												<td><?= $row->tahun; ?></td>
												<td><?= $row->bulan; ?></td>
												<td><?= $row->tgl_bayar; ?></td>
												<td><?= $row->terbayar; ?></td>
												<td>
													<label class="badge <?= $row->status === 'paid' ? 'badge-success' : 'badge-danger'; ?>"><?= $row->status === 'paid' ? 'Lunas' : 'Belum Lunas' ?></label>
												</td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var date = new Date()
	$('#pay-button').click(function(event) {
		event.preventDefault();

		$.ajax({
			url: '<?= site_url() ?>/snap/token',
			type: 'post',
			data: {
				nama: $("#nama").val(),
				alamat: `<?= $siswa->alamat; ?>`,
				no_hp: `<?= $siswa->no_hp; ?>`,
				total: $("#tagihan").val(),
				nisn: `<?= $siswa->nisn; ?>`,
				bulan: $("#bulan").val(),
			},
			cache: false,

			success: function(data) {
				//location = data;
				// $(this).attr("disabled", "disabled");

				console.log('token = ' + data);

				var resultType = document.getElementById('result-type');
				var resultData = document.getElementById('result-data');

				function changeResult(type, data) {
					$("#result-type").val(type);
					$("#result-data").val(JSON.stringify(data));

					$("#orderid").val(data.order_id)
					$("#transaksi_id").val(data.transaction_id)
					$("#metode_pembayaran").val(data.payment_type)
					$("#bank").val(data.va_numbers[0].bank)
					$("#kode_pembayaran").val(data.va_numbers[0].va_number)
					$("#tanggal_transaksi").val(data.transaction_time)
					$("#status").val(data.transaction_status)
					// console.log(data)
					//resultType.innerHTML = type;
					//resultData.innerHTML = JSON.stringify(data);
				}

				snap.pay(data, {

					onSuccess: function(result) {
						changeResult('success', result);
						console.log(result.status_message);
						console.log(result);
						$("#payment_form").submit();
					},
					onPending: function(result) {
						changeResult('pending', result);
						console.log(result.status_message);
						// return false;
						$("#payment_form").submit();
					},
					onError: function(result) {
						changeResult('error', result);
						console.log(result.status_message);
						$("#payment_form").submit();
					}
				});
			}
		});
	});

	$("#byr_bulan").change(function() {
		$.ajax({
			url: `<?= site_url() ?>` + 'Siswa/Pembayaran/getPembayaran',
			type: 'POST',
			dataType: 'json',
			data: {
				id_pembayaran: $(this).val(),
				nisn: <?= $siswa->nisn ?>
			},
			success: function(data) {
				console.log(data)
				$("#tagihan").val((data[0].tagihan - data[0].terbayar))
				$("input[name='tagihan']").val(data[0].tagihan)
				$("#id").val(data[0].id_pembayaran)
				$("#bulan").val(data[0].bulan + ' ' + date.getFullYear())
			}
		})
	})
</script>