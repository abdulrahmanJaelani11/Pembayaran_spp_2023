<div class="container-scroller">
	<div class="container-fluid page-body-wrapper full-page-wrapper">
		<div class="content-wrapper d-flex align-items-center auth">
			<div class="row flex-grow">
				<div class="col-lg-4 mx-auto">
					<div class="auth-form-light text-left p-5">
						<div class="brand-logo">
							<img src="<?= base_url(); ?>assets_me/images/logo-dark.svg">
						</div>
						<h4>Selamat Datang</h4>
						<h6 class="font-weight-light">Silahkan login untuk melanjutkan</h6>

						<form class="pt-3" action="<?= base_url('Siswa/Auth'); ?>" method="POST">
							<div class="form-group">
								<input type="nisn" class="form-control form-control-lg" name="nisn" id="nisn" value="<?= set_value('nisn'); ?>" placeholder="Masukan NISN">
								<?= form_error('nisn', '<small class="text-danger">', '</small>') ?>
							</div>
							<div class="form-group">
								<input type="password" class="form-control form-control-lg" name="password" id="password" placeholder="Password">
								<?= form_error('password', '<small class="text-danger">', '</small>') ?>
							</div>
							<div class="mt-3">
								<button type="submit" name="login" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn"> Login </button>
							</div>
							<div class="my-2 d-flex justify-content-between align-items-center">
								<a href="<?= base_url('Siswa/Auth/lupa_password'); ?>" class="auth-link text-black">Lupa Password?</a>
								<a href="<?= base_url('Admin/Auth'); ?>" class="auth-link text-black">Login Admin</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- content-wrapper ends -->
	</div>
	<!-- page-body-wrapper ends -->
</div>