<style>
    table td {
        font-size: 13px;
    }
</style>

<div class="card text-dark">
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div class="d-sm-flex justify-content-between align-items-center transaparent-tab-border {">
                    <ul class="nav nav-tabs tab-transparent" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="siswa-det-tab" data-toggle="tab" href="#siswa-detail" role="tab" aria-selected="true">Profil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="business-tab" data-toggle="tab" href="#business-1" role="tab" aria-selected="false">Edit Akun</a>
                        </li>
                    </ul>
                    <div class="d-md-block d-none">
                        <a href="#" class="text-light p-1"><i class="mdi mdi-view-dashboard"></i></a>
                        <a href="#" class="text-light p-1"><i class="mdi mdi-dots-vertical"></i></a>
                    </div>
                </div>
                <div class="tab-content tab-transparent-content">
                    <div class="tab-pane fade show active" id="siswa-detail" role="tabpanel" aria-labelledby="siswa-det-tab">
                        <div class="row">
                            <div class="col-md-4">
                                <img src="<?= base_url('assets_me/images/profile/default.png'); ?>" class="img-fluid img-thumbnail" alt="">
                            </div>
                            <div class="col-md-8">
                                <div class="p-2">
                                    <table>
                                        <tr>
                                            <td class="pt-2">Nama</td>
                                            <td class="pt-2 pl-2">:</td>
                                            <td class="pt-2 pl-2"><?= $siswa->nama; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="pt-2">NISN</td>
                                            <td class="pt-2 pl-2">:</td>
                                            <td class="pt-2 pl-2"><?= $siswa->nisn; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="pt-2">Jenis Kelamin</td>
                                            <td class="pt-2 pl-2">:</td>
                                            <td class="pt-2 pl-2"><?= $siswa->jenis_kelamin; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="pt-2">Kelas</td>
                                            <td class="pt-2 pl-2">:</td>
                                            <td class="pt-2 pl-2"><?= $siswa->kelas; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="pt-2">Jurusan</td>
                                            <td class="pt-2 pl-2">:</td>
                                            <td class="pt-2 pl-2"><?= $siswa->jurusan; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="pt-2">Agama</td>
                                            <td class="pt-2 pl-2">:</td>
                                            <td class="pt-2 pl-2"><?= $siswa->agama; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="pt-2">Tanggal Lahir</td>
                                            <td class="pt-2 pl-2">:</td>
                                            <td class="pt-2 pl-2"><?= $siswa->tanggal_lahir; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="pt-2">Tempat Lahir</td>
                                            <td class="pt-2 pl-2">:</td>
                                            <td class="pt-2 pl-2"><?= $siswa->tempat_lahir; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="pt-2">NO. Telepon</td>
                                            <td class="pt-2 pl-2">:</td>
                                            <td class="pt-2 pl-2"><?= $siswa->no_hp; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="pt-2">Golongan darah</td>
                                            <td class="pt-2 pl-2">:</td>
                                            <td class="pt-2 pl-2"><?= $siswa->golongan_darah; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="pt-2">Riwayat Penyakit</td>
                                            <td class="pt-2 pl-2">:</td>
                                            <td class="pt-2 pl-2"><?= $siswa->riwayat_penyakit; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="pt-2">Alamat</td>
                                            <td class="pt-2 pl-2">:</td>
                                            <td class="pt-2 pl-2"><?= $siswa->alamat; ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show" id="business-1" role="tabpanel" aria-labelledby="business-tab">
                        <div class="row justify-content-center">
                            <div class="col-lg-6 mt-4">
                                <form action="<?= base_url('Siswa/Auth/ubah_password'); ?>" method="post">
                                    <div class="form-group">
                                        <label for="password_sekarang">Password Saat Ini</label>
                                        <input type="password" autocomplete="off" name="password_sekarang" id="password_sekarang" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="password_baru">Password Baru</label>
                                        <input type="password" autocomplete="off" name="password_baru" id="password_baru" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="password_konfirmasi">Konfirmasi password</label>
                                        <input type="password" autocomplete="off" name="password_konfirmasi" id="password_konfirmasi" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" name="simpan" class="btn btn-primary"> Simpan </button>
                                        <button type="reset" class="btn btn-danger"> Reset </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>