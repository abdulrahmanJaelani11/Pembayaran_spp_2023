<div class="alert alert-success"><?= $siswa->nama ?></div>
<div class="row">
	<div class="col-xl-3 col-lg-6 col-sm-6 grid-margin stretch-card">
		<div class="card" style="">
			<div class="card-body text-center">
				<h5 class="mb-2 text-dark font-weight-normal">Total Bulan terbayar</h5>
				<h2 class="mb-4 text-dark font-weight-bold"><?= $bulan__terbayar; ?> Bulan</h2>
				<div class="dashboard-progress dashboard-progress-1 d-flex align-items-center justify-content-center item-parent"><canvas width="125" height="125"></canvas><i class="mdi mdi-calendar-text icon-md absolute-center text-dark"></i></div>
				<p class="mt-4 mb-0">Nominal</p>
				<h3 class="mb-0 font-weight-bold mt-2 text-dark">RP. <?= number_format($nominal__terbayar, 0, '.', ','); ?></h3>
			</div>
		</div>
	</div>
	<div class="col-xl-3 col-lg-6 col-sm-6 grid-margin stretch-card">
		<div class="card">
			<div class="card-body text-center">
				<h5 class="mb-2 text-dark font-weight-normal">Sisa Bulan</h5>
				<h2 class="mb-4 text-dark font-weight-bold"><?= 12 - $bulan__terbayar; ?> Bulan</h2>
				<div class="dashboard-progress dashboard-progress-1 d-flex align-items-center justify-content-center item-parent"><canvas width="125" height="125"></canvas><i class="mdi mdi-calendar-text icon-md absolute-center text-dark"></i></div>
				<p class="mt-4 mb-0">Nominal</p>
				<h3 class="mb-0 font-weight-bold mt-2 text-dark">RP. <?= number_format($nominal_belum_terbayar, 0, '.', ','); ?></h3>
			</div>
		</div>
	</div>
</div>