<style>
	.card:hover {
		box-shadow: 0 0 20px silver;
	}

	table td {
		padding: 2px 4px;
	}
</style>

<div class="row">
	<div class="col-12">
		<h3>Riwayat Pembayaran</h3>
		<hr>
	</div>

	<?php foreach ($list_pembayaran as $row) : ?>
		<div class="col-xl-3 col-lg-6 grid-margin">
			<?php if ($row->metode_pembayaran != '') : ?>
				<a id="card-link" href="<?= base_url('Siswa/Pembayaran/history_det/' . $row->id_pembayaran); ?>" style="text-decoration: none;">
				<?php endif ?>
				<div class="card">
					<div class="card-body text-dark">
						<table>
							<tr>
								<td>Tahun</td>
								<td>:</td>
								<td><?= $row->tahun; ?></td>
							</tr>
							<tr>
								<td>Bulan</td>
								<td>:</td>
								<td><?= $row->bulan; ?></td>
							</tr>
							<tr>
								<td>Status</td>
								<td>:</td>
								<td><label class="badge <?= $row->status === 'paid' ? 'badge-success' : 'badge-danger'; ?>"><?= $row->status === 'paid' ? 'Lunas' : 'Belum Lunas' ?></label></td>
							</tr>
							<tr>
								<td>Terbayar</td>
								<td>:</td>
								<td><?= $row->terbayar ?></td>
							</tr>
							<tr>
								<td>Tanggal Bayar</td>
								<td>:</td>
								<td><?= $row->tgl_bayar; ?></td>
							</tr>
						</table>
					</div>
				</div>
				<?php if ($row->metode_pembayaran != '') : ?>
				</a>
			<?php endif ?>
		</div>
	<?php endforeach; ?>
</div>