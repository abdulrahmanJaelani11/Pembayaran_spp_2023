<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="description" content="">
	<meta name="author" content="">

	<title>Easy-Pay</title>

	<!-- CSS FILES -->
	<link href="<?= base_url('assets_front'); ?>/css/bootstrap.min.css" rel="stylesheet">

	<link href="<?= base_url('assets_front'); ?>/css/bootstrap-icons.css" rel="stylesheet">

	<link href="<?= base_url('assets_front'); ?>/css/templatemo-kind-heart-charity.css" rel="stylesheet">
	<!--

TemplateMo 581 Kind Heart Charity

https://templatemo.com/tm-581-kind-heart-charity

-->

</head>

<body id="section_1">

	<header class="site-header">
		<div class="container">
			<div class="row">

				<div class="col-lg-8 col-12 d-flex flex-wrap">
					<p class="d-flex me-4 mb-0">
						<i class="bi-geo-alt me-2"></i>
						Kp. Cikenceh 2 Desa Harumansari Kec. Kadungora
					</p>

					<p class="d-flex mb-0">
						<i class="bi-envelope me-2"></i>

						<a href="mailto:randikaangga9044@gmail.com">
							randikaangga9044@gmail.com
						</a>
					</p>
				</div>

				<div class="col-lg-3 col-12 ms-auto d-lg-block d-none">
					<ul class="social-icon">
						<!-- <li class="social-icon-item">
							<a href="#" class="social-icon-link bi-twitter"></a>
						</li> -->

						<li class="social-icon-item">
							<a href="https://www.facebook.com/profile.php?id=100025116722709" class="social-icon-link bi-facebook"></a>
						</li>

						<li class="social-icon-item">
							<a href="https://www.instagram.com/abdurahman_jaelani/" class="social-icon-link bi-instagram"></a>
						</li>

						<!-- <li class="social-icon-item">
							<a href="#" class="social-icon-link bi-youtube"></a>
						</li> -->

						<li class="social-icon-item">
							<a href="#" class="social-icon-link bi-whatsapp"></a>
						</li>
					</ul>
				</div>

			</div>
		</div>
	</header>

	<nav class="navbar navbar-expand-lg bg-light shadow-lg">
		<div class="container">
			<a class="navbar-brand" href="<?= base_url() ?>">
				<img src="<?= base_url('assets_front'); ?>/images/logo.png" class="logo img-fluid" alt="Kind Heart Charity">
				<span>
					Easy-Pay App
					<small>Aplikasi Pembayaran SPP</small>
				</span>
			</a>

			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ms-auto">
					<li class="nav-item">
						<a class="nav-link" href="#top">Beranda</a>
					</li>

					<li class="nav-item" style="display: none;">
						<a class="nav-link click-scroll" href="#section_2">Tentang Kami</a>
					</li>

					<li class="nav-item" style="display: none;">
						<a class="nav-link click-scroll" href="#section_3">Causes</a>
					</li>

					<!-- <li class="nav-item">
						<a class="nav-link click-scroll" href="#section_4">Volunteer</a>
					</li> -->

					<li class="nav-item">
						<a class="nav-link click-scroll" href="#section_2" id="">Tentang Kami</a>

						<!-- <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarLightDropdownMenuLink">
							<li><a class="dropdown-item" href="news.html">Daftar Berita</a></li>

							<li><a class="dropdown-item" href="news-detail.html">Rincian Berita</a></li>
						</ul> -->
					</li>

					<li class="nav-item" style="display: none;">
						<a class="nav-link click-scroll" href="#section_6">Kontak</a>
					</li>

					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" id="btn_login" role="button" data-bs-toggle="dropdown">Login</a>

						<ul id="option_login" class="dropdown-menu dropdown-menu" aria-labelledby="btn_login">
							<li><a class="dropdown-item" href="<?= base_url('Admin/Auth'); ?>">Administrator</a></li>

							<li><a class="dropdown-item" href="<?= base_url('Siswa/Auth'); ?>">Siswa</a></li>
						</ul>
					</li>

				</ul>
			</div>
		</div>
	</nav>

	<main>

		<section class="hero-section hero-section-full-height">
			<div class="container-fluid">
				<div class="row">

					<div class="col-lg-12 col-12 p-0">
						<div id="hero-slide" class="carousel carousel-fade slide" data-bs-ride="carousel">
							<div class="carousel-inner">
								<div class="carousel-item active">
									<img src="<?= base_url('assets_front'); ?>/images/slide/slide1.jpg" class="carousel-image img-fluid" alt="...">

									<div class="carousel-caption d-flex flex-column justify-content-end">
										<h1>be a Kind Heart</h1>

										<p>Professional charity theme based on Bootstrap 5.2.2</p>
									</div>
								</div>

								<div class="carousel-item">
									<img src="<?= base_url('assets_front'); ?>/images/slide/slide2.jpg" class="carousel-image img-fluid" alt="...">

									<div class="carousel-caption d-flex flex-column justify-content-end">
										<h1>Non-profit</h1>

										<p>You can support us to grow more</p>
									</div>
								</div>

								<div class="carousel-item">
									<img src="<?= base_url('assets_front'); ?>/images/slide/slide3.jpg" class="carousel-image img-fluid" alt="...">

									<div class="carousel-caption d-flex flex-column justify-content-end">
										<h1>Humanity</h1>

										<p>Please tell your friends about our website</p>
									</div>
								</div>
							</div>

							<button class="carousel-control-prev" type="button" data-bs-target="#hero-slide" data-bs-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="visually-hidden">Previous</span>
							</button>

							<button class="carousel-control-next" type="button" data-bs-target="#hero-slide" data-bs-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="visually-hidden">Next</span>
							</button>
						</div>
					</div>

				</div>
			</div>
		</section>


		<section class="section-padding">
			<div class="container">
				<div class="row">

					<div class="col-lg-10 col-12 text-center mx-auto">
						<h2 class="mb-5">Welcome to Kind Heart Charity</h2>
					</div>

					<div class="col-lg-3 col-md-6 col-12 mb-4 mb-lg-0">
						<div class="featured-block d-flex justify-content-center align-items-center">
							<a href="donate.html" class="d-block">
								<img src="<?= base_url('assets_front'); ?>/images/icons/hands.png" class="featured-block-image img-fluid" alt="">

								<p class="featured-block-text">Become a <strong>volunteer</strong></p>
							</a>
						</div>
					</div>

					<div class="col-lg-3 col-md-6 col-12 mb-4 mb-lg-0 mb-md-4">
						<div class="featured-block d-flex justify-content-center align-items-center">
							<a href="donate.html" class="d-block">
								<img src="<?= base_url('assets_front'); ?>/images/icons/heart.png" class="featured-block-image img-fluid" alt="">

								<p class="featured-block-text"><strong>Caring</strong> Earth</p>
							</a>
						</div>
					</div>

					<div class="col-lg-3 col-md-6 col-12 mb-4 mb-lg-0 mb-md-4">
						<div class="featured-block d-flex justify-content-center align-items-center">
							<a href="donate.html" class="d-block">
								<img src="<?= base_url('assets_front'); ?>/images/icons/receive.png" class="featured-block-image img-fluid" alt="">

								<p class="featured-block-text">Make a <strong>Donation</strong></p>
							</a>
						</div>
					</div>

					<div class="col-lg-3 col-md-6 col-12 mb-4 mb-lg-0">
						<div class="featured-block d-flex justify-content-center align-items-center">
							<a href="donate.html" class="d-block">
								<img src="<?= base_url('assets_front'); ?>/images/icons/scholarship.png" class="featured-block-image img-fluid" alt="">

								<p class="featured-block-text"><strong>Scholarship</strong> Program</p>
							</a>
						</div>
					</div>

				</div>
			</div>
		</section>

		<section class="section-padding section-bg" id="section_2">
			<div class="container">
				<div class="row">

					<div class="col-lg-6 col-12 mb-5 mb-lg-0">
						<img src="<?= base_url('assets_front'); ?>/images/group-people-volunteering-foodbank-poor-people.jpg" class="custom-text-box-image img-fluid" alt="">
					</div>

					<div class="col-lg-6 col-12">
						<div class="custom-text-box">
							<h2 class="mb-2">Our Story</h2>

							<h5 class="mb-3">Kind Heart Charity, Non-Profit Organization</h5>

							<p class="mb-0">This is a Bootstrap 5.2.2 CSS template for charity organization websites.
								You can feel free to use it. Please tell your friends about TemplateMo website. Thank
								you.</p>
						</div>

						<div class="row">
							<div class="col-lg-6 col-md-6 col-12">
								<div class="custom-text-box mb-lg-0">
									<h5 class="mb-3">Our Mission</h5>

									<p>Sed leo nisl, posuere at molestie ac, suscipit auctor quis metus</p>

									<ul class="custom-list mt-2">
										<li class="custom-list-item d-flex">
											<i class="bi-check custom-text-box-icon me-2"></i>
											Charity Theme
										</li>

										<li class="custom-list-item d-flex">
											<i class="bi-check custom-text-box-icon me-2"></i>
											Semantic HTML
										</li>
									</ul>
								</div>
							</div>

							<div class="col-lg-6 col-md-6 col-12">
								<div class="custom-text-box d-flex flex-wrap d-lg-block mb-lg-0">
									<div class="counter-thumb">
										<div class="d-flex">
											<span class="counter-number" data-from="1" data-to="2009" data-speed="1000"></span>
											<span class="counter-number-text"></span>
										</div>

										<span class="counter-text">Founded</span>
									</div>

									<div class="counter-thumb mt-4">
										<div class="d-flex">
											<span class="counter-number" data-from="1" data-to="120" data-speed="1000"></span>
											<span class="counter-number-text">B</span>
										</div>

										<span class="counter-text">Donations</span>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</section>


		<section class="testimonial-section section-padding section-bg">
			<div class="container">
				<div class="row">

					<div class="col-lg-8 col-12 mx-auto">
						<h2 class="mb-lg-3">Happy customers</h2>

						<div id="testimonial-carousel" class="carousel carousel-fade slide" data-bs-ride="carousel">

							<div class="carousel-inner">
								<div class="carousel-item active">
									<div class="carousel-caption">
										<h4 class="carousel-title">Lorem Ipsum dolor sit amet, consectetur adipsicing
											kengan omeg kohm tokito charity theme</h4>

										<small class="carousel-name"><span class="carousel-name-title">Maria</span>,
											Boss</small>
									</div>
								</div>

								<div class="carousel-item">
									<div class="carousel-caption">
										<h4 class="carousel-title">Sed leo nisl, posuere at molestie ac, suscipit auctor
											mauris quis metus tempor orci</h4>

										<small class="carousel-name"><span class="carousel-name-title">Thomas</span>,
											Partner</small>
									</div>
								</div>

								<div class="carousel-item">
									<div class="carousel-caption">
										<h4 class="carousel-title">Lorem Ipsum dolor sit amet, consectetur adipsicing
											kengan omeg kohm tokito charity theme</h4>

										<small class="carousel-name"><span class="carousel-name-title">Jane</span>,
											Advisor</small>
									</div>
								</div>

								<div class="carousel-item">
									<div class="carousel-caption">
										<h4 class="carousel-title">Sed leo nisl, posuere at molestie ac, suscipit auctor
											mauris quis metus tempor orci</h4>

										<small class="carousel-name"><span class="carousel-name-title">Bob</span>,
											Entreprenuer</small>
									</div>
								</div>

								<ol class="carousel-indicators">
									<li data-bs-target="#testimonial-carousel" data-bs-slide-to="0" class="active">
										<img src="<?= base_url('assets_front'); ?>/images/avatar/portrait-beautiful-young-woman-standing-grey-wall.jpg" class="img-fluid rounded-circle avatar-image" alt="avatar">
									</li>

									<li data-bs-target="#testimonial-carousel" data-bs-slide-to="1" class="">
										<img src="<?= base_url('assets_front'); ?>/images/avatar/portrait-young-redhead-bearded-male.jpg" class="img-fluid rounded-circle avatar-image" alt="avatar">
									</li>

									<li data-bs-target="#testimonial-carousel" data-bs-slide-to="2" class="">
										<img src="<?= base_url('assets_front'); ?>/images/avatar/pretty-blonde-woman-wearing-white-t-shirt.jpg" class="img-fluid rounded-circle avatar-image" alt="avatar">
									</li>

									<li data-bs-target="#testimonial-carousel" data-bs-slide-to="3" class="">
										<img src="<?= base_url('assets_front'); ?>/images/avatar/studio-portrait-emotional-happy-funny.jpg" class="img-fluid rounded-circle avatar-image" alt="avatar">
									</li>
								</ol>

							</div>
						</div>
					</div>

				</div>
			</div>
		</section>


		<section class="contact-section section-padding" id="section_6">
			<div class="container">
				<div class="row">

					<div class="col-lg-4 col-12 ms-auto mb-5 mb-lg-0">
						<div class="contact-info-wrap">
							<h2>Get in touch</h2>

							<div class="contact-image-wrap d-flex flex-wrap">
								<img src="<?= base_url('assets_front'); ?>/images/avatar/pretty-blonde-woman-wearing-white-t-shirt.jpg" class="img-fluid avatar-image" alt="">

								<div class="d-flex flex-column justify-content-center ms-3">
									<p class="mb-0">Clara Barton</p>
									<p class="mb-0"><strong>HR & Office Manager</strong></p>
								</div>
							</div>

							<div class="contact-info">
								<h5 class="mb-3">Contact Infomation</h5>

								<p class="d-flex mb-2">
									<i class="bi-geo-alt me-2"></i>
									Akershusstranda 20, 0150 Oslo, Norway
								</p>

								<p class="d-flex mb-2">
									<i class="bi-telephone me-2"></i>

									<a href="tel: 305-240-9671">
										305-240-9671
									</a>
								</p>

								<p class="d-flex">
									<i class="bi-envelope me-2"></i>

									<a href="mailto:info@yourgmail.com">
										donate@charity.org
									</a>
								</p>

								<a href="#" class="custom-btn btn mt-3">Get Direction</a>
							</div>
						</div>
					</div>

					<div class="col-lg-5 col-12 mx-auto">
						<form class="custom-form contact-form" action="#" method="post" role="form">
							<h2>Contact form</h2>

							<p class="mb-4">Or, you can just send an email:
								<a href="#">info@charity.org</a>
							</p>
							<div class="row">
								<div class="col-lg-6 col-md-6 col-12">
									<input type="text" name="first-name" id="first-name" class="form-control" placeholder="Jack" required>
								</div>

								<div class="col-lg-6 col-md-6 col-12">
									<input type="text" name="last-name" id="last-name" class="form-control" placeholder="Doe" required>
								</div>
							</div>

							<input type="email" name="email" id="email" pattern="[^ @]*@[^ @]*" class="form-control" placeholder="Jackdoe@gmail.com" required>

							<textarea name="message" rows="5" class="form-control" id="message" placeholder="What can we help you?"></textarea>

							<button type="submit" class="form-control">Send Message</button>
						</form>
					</div>

				</div>
			</div>
		</section>
	</main>

	<footer class="site-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-12 mb-4">
					<img src="<?= base_url('assets_front'); ?>/images/logo.png" class="logo img-fluid" alt="">
				</div>

				<div class="col-lg-4 col-md-6 col-12 mb-4">
					<h5 class="site-footer-title mb-3">Quick Links</h5>

					<ul class="footer-menu">
						<li class="footer-menu-item"><a href="#" class="footer-menu-link">Our Story</a></li>

						<li class="footer-menu-item"><a href="#" class="footer-menu-link">Newsroom</a></li>

						<li class="footer-menu-item"><a href="#" class="footer-menu-link">Causes</a></li>

						<li class="footer-menu-item"><a href="#" class="footer-menu-link">Become a volunteer</a></li>

						<li class="footer-menu-item"><a href="#" class="footer-menu-link">Partner with us</a></li>
					</ul>
				</div>

				<div class="col-lg-4 col-md-6 col-12 mx-auto">
					<h5 class="site-footer-title mb-3">Contact Infomation</h5>

					<p class="text-white d-flex mb-2">
						<i class="bi-telephone me-2"></i>

						<a href="tel: 305-240-9671" class="site-footer-link">
							305-240-9671
						</a>
					</p>

					<p class="text-white d-flex">
						<i class="bi-envelope me-2"></i>

						<a href="mailto:info@yourgmail.com" class="site-footer-link">
							donate@charity.org
						</a>
					</p>

					<p class="text-white d-flex mt-3">
						<i class="bi-geo-alt me-2"></i>
						Akershusstranda 20, 0150 Oslo, Norway
					</p>

					<a href="#" class="custom-btn btn mt-3">Get Direction</a>
				</div>
			</div>
		</div>

		<div class="site-footer-bottom">
			<div class="container">
				<div class="row">

					<div class="col-lg-6 col-md-7 col-12">
						<p class="copyright-text mb-0">Copyright © 2036 <a href="#">Kind Heart</a> Charity Org.
							Design: <a href="https://templatemo.com" target="_blank">TemplateMo</a><br>Distribution:
							<a href="https://themewagon.com">ThemeWagon</a>
						</p>
					</div>

					<div class="col-lg-6 col-md-5 col-12 d-flex justify-content-center align-items-center mx-auto">
						<ul class="social-icon">
							<li class="social-icon-item">
								<a href="#" class="social-icon-link bi-twitter"></a>
							</li>

							<li class="social-icon-item">
								<a href="#" class="social-icon-link bi-facebook"></a>
							</li>

							<li class="social-icon-item">
								<a href="#" class="social-icon-link bi-instagram"></a>
							</li>

							<li class="social-icon-item">
								<a href="#" class="social-icon-link bi-linkedin"></a>
							</li>

							<li class="social-icon-item">
								<a href="https://youtube.com/templatemo" class="social-icon-link bi-youtube"></a>
							</li>
						</ul>
					</div>

				</div>
			</div>
		</div>
	</footer>

	<!-- JAVASCRIPT FILES -->
	<script src="<?= base_url('assets_front'); ?>/js/jquery.min.js"></script>
	<script src="<?= base_url('assets_front'); ?>/js/bootstrap.min.js"></script>
	<script src="<?= base_url('assets_front'); ?>/js/jquery.sticky.js"></script>
	<script src="<?= base_url('assets_front'); ?>/js/click-scroll.js"></script>
	<script src="<?= base_url('assets_front'); ?>/js/counter.js"></script>
	<script src="<?= base_url('assets_front'); ?>/js/custom.js"></script>
	<script>
		console.log(screen.width)
		let screen_width = screen.width
		if (screen_width < 400) {
			show_btn_login()
		}

		function show_btn_login() {
			$("#btn_login").click(function() {
				$("#option_login").slideToggle()
			})
		}
	</script>

</body>

</html>