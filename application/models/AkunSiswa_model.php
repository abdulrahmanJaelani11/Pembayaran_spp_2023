<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AkunSiswa_model extends CI_Model
{
	public function save($data)
	{
		return $this->db->insert('ref_akun_siswa', $data);
	}

	public function getAkun($nisn = null)
	{
		$data = $this->db->get_where('ref_akun_siswa', ['nisn' => $nisn])->row();
		return $data;
	}

	public function update($nisn, $data)
	{
		$this->db->where('nisn', $nisn);
		return $this->db->update('ref_akun_siswa', $data);
	}
}


/* End of file AkunSiswa_model.php and path \application\models\AkunSiswa_model.php */
