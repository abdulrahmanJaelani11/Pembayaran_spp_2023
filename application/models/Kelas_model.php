<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelas_model extends CI_Model
{
	public function getAllKelas()
	{
		return $this->db->get('ref_kelas')->result();
	}

	public function getKelas($id_kelas)
	{
		$this->db->select('*');
		$this->db->where('id_kelas', $id_kelas);
		$result = $this->db->get('ref_kelas')->row();
		return $result;
	}

	public function save($data)
	{
		return $this->db->insert('ref_kelas', $data);
	}

	public function update($id, $data)
	{
		$this->db->where('id_kelas', $id);
		return $this->db->update('ref_kelas', $data);
	}

	public function delete($id)
	{
		$this->db->where(['id_kelas' => $id]);
		return $this->db->delete('ref_kelas');
	}
}


/* End of file Kelas_model.php and path \application\models\Kelas_model.php */
