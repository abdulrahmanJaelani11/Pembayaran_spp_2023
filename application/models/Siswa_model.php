<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Siswa_model extends CI_Model
{
	public function getAllSiswa()
	{
		// return $this->db->get('ref_siswa')->result();
		$this->db->select("a.*, b.kelas, c.jurusan, c.singkatan");
		$this->db->from('ref_siswa a');
		$this->db->join('ref_kelas b', 'a.id_kelas = b.id_kelas');
		$this->db->join('ref_jurusan c', 'a.id_jurusan = c.id_jurusan');
		$this->db->where('a.aktif', 1);
		$this->db->order_by('a.nama', "ASC");
		$result = $this->db->get()->result();
		return $result;
	}

	public function getSiswa($id_kelas)
	{
		$this->db->select("a.*, b.kelas, c.jurusan, c.singkatan");
		$this->db->from('ref_siswa a');
		$this->db->join('ref_kelas b', 'a.id_kelas = b.id_kelas');
		$this->db->join('ref_jurusan c', 'a.id_jurusan = c.id_jurusan');
		$this->db->where('a.aktif', 1);
		$this->db->where('a.id_kelas', $id_kelas);
		$this->db->order_by('a.nama', "ASC");
		$result = $this->db->get()->result();
		return $result;
	}

	public function getSiswaJk($filter)
	{
		$hasil = $this->db->get_where('ref_siswa', $filter)->result_array();
		return $hasil;
	}

	public function getPencarian($key, $id)
	{
		$this->db->select('a.*, b.kelas, c.jurusan, c.singkatan');
		$this->db->from('ref_siswa a');
		$this->db->join('ref_kelas b', 'a.id_kelas=b.id_kelas');
		$this->db->join('ref_jurusan c', 'a.id_jurusan=c.id_jurusan');
		$this->db->where('a.id_kelas', $id);
		$this->db->like('a.nama', $key);
		$data = $this->db->get()->result();
		return $data;
	}

	public function save($data)
	{
		$this->db->insert('ref_siswa', $data);
		$last_query = $this->db->insert_id();
		return $last_query;
	}

	public function find($nisn)
	{
		$this->db->select('a.*, b.kelas, c.jurusan');
		$this->db->from('ref_siswa a');
		$this->db->join('ref_kelas b', 'a.id_kelas=b.id_kelas');
		$this->db->join('ref_jurusan c', 'a.id_jurusan=c.id_jurusan');
		$this->db->where('a.nisn', $nisn);
		$data = $this->db->get()->result();
		if ($data) {
			$data = $data[0];
		}
		return $data;
	}

	public function update_siswa($id_siswa, $data)
	{
		$this->db->where(['id' => $id_siswa]);
		return $this->db->update('ref_siswa', $data);
	}

	public function delete_siswa($id_siswa)
	{
		$this->db->where(['id' => $id_siswa]);
		return $this->db->delete('ref_siswa');
	}
}


/* End of file Siswa_model.php and path \application\models\Siswa_model.php */
