<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jurusan_model extends CI_Model
{
	public function getAllJurusan()
	{
		return $this->db->get('ref_jurusan')->result();
	}

	public function getJurusan($id)
	{
		$this->db->where('id_jurusan', $id);
		return $this->db->get('ref_jurusan')->row();
	}

	public function save($data)
	{
		return $this->db->insert('ref_jurusan', $data);
	}

	public function update($id, $data)
	{
		$this->db->where('id_jurusan', $id);
		return $this->db->update('ref_jurusan', $data);
	}

	function delete($id)
	{
		$this->db->where('id_jurusan', $id);
		return $this->db->delete('ref_jurusan');
	}
}


/* End of file Jurusan_model.php and path \application\models\Jurusan_model.php */
