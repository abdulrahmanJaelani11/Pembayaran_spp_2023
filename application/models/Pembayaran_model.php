<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembayaran_model extends CI_Model
{
	public function select()
	{
	}

	public function addPembayaran($data)
	{
		// echo "<pre>";
		// var_dump($data);
		// echo "</pre>";
		// die;
		return $this->db->insert_batch('pembayaran', $data);
	}

	public function getPembayaran($nisn = null, $form = null, $id_pem = null)
	{
		if ($nisn != null && $id_pem != null) {
			$filter = ['pembayaran.nisn' => $nisn, 'pembayaran.id_pembayaran' => $id_pem];
		}
		if ($nisn != null && $id_pem == null) {
			$filter = ['pembayaran.nisn' => $nisn, 'pembayaran.status' => 'pending'];
		}
		$this->db->join('ref_siswa', 'ref_siswa.nisn=pembayaran.nisn');
		$this->db->join('ref_kelas', 'ref_kelas.id_kelas=ref_siswa.id_kelas');
		$this->db->join('ref_jurusan', 'ref_jurusan.id_jurusan=ref_siswa.id_jurusan');
		$data = $this->db->get_where('pembayaran', $filter)->result();
		if (!$data) {
			$data = ['status' => 201];
		}
		return $data;
	}

	public function pembayaranBulanIni($nisn)
	{
		$this->db->join('ref_siswa', 'ref_siswa.nisn=pembayaran.nisn');
		$this->db->join('ref_kelas', 'ref_siswa.id_kelas=ref_kelas.id_kelas');
		$this->db->join('ref_jurusan', 'ref_siswa.id_jurusan=ref_jurusan.id_jurusan');
		return $this->db->get_where('pembayaran', ['index_bulan' => date('m'), 'pembayaran.nisn' => $nisn])->row();
	}

	public function terbayar($nisn)
	{

		return $this->db->get_where('pembayaran', ['status' => 'paid', 'nisn' => $nisn])->result();
	}

	public function getAllPembayaran($nisn = null, $list_history = false)
	{
		if ($list_history != false) {
			$this->db->select('a.*');
			$this->db->from('pembayaran a');
			$this->db->where('nisn', $nisn);
			$this->db->where('terbayar !=', '');
			$data = $this->db->get()->result();
		} else {
			$data = $this->db->get_where('pembayaran', ['nisn' => $nisn])->result();
		}
		return $data;
	}

	function getAllpembayaranPaid()
	{
		$this->db->select('a.*, b.*, c.*, d.*');
		$this->db->from('pembayaran a');
		$this->db->join('ref_siswa b', 'a.nisn=b.nisn');
		$this->db->join('ref_kelas c', 'b.id_kelas=c.id_kelas');
		$this->db->join('ref_jurusan d', 'b.id_jurusan=d.id_jurusan');
		$this->db->where('a.terbayar !=', '');
		$this->db->where('MONTH(a.tgl_bayar)', date('m'));
		$this->db->where('a.aktif', 1);
		$this->db->order_by('a.tgl_bayar', 'DESC');
		$data = $this->db->get()->result();
		return $data;
	}

	function getPemBelumTerbayar($nisn = null, $periode = false)
	{
		$this->db->select('a.*, b.*, c.*, d.*');
		$this->db->from('pembayaran a');
		$this->db->join('ref_siswa b', 'a.nisn=b.nisn');
		$this->db->join('ref_kelas c', 'b.id_kelas=c.id_kelas');
		$this->db->join('ref_jurusan d', 'b.id_jurusan=d.id_jurusan');
		$this->db->where('a.status', 'pending');
		$this->db->where('a.aktif', 1);
		if ($nisn != null) {
			$this->db->where('a.nisn', $nisn);
		}
		if ($periode != false) {
			$this->db->where('a.index_bulan <=', date('m'));
			$this->db->where('a.status', 'pending');
		}
		$this->db->order_by('a.tgl_bayar', 'DESC');
		$data = $this->db->get()->result();
		return $data;
	}

	public function save($data, $filter)
	{
		$this->db->where($filter);
		return $this->db->update('pembayaran', $data);
	}

	public function ubah_tagihan($id_pem = null, $data)
	{
		$filter = '';

		return $this->db->query("UPDATE pembayaran SET `tagihan` = $data $filter");
	}

	public function getPembayaranByOrderId($order_id)
	{
		return $this->db->get_where('pembayaran', ['order_id' => $order_id])->row();
	}

	public function update($id_pem = null, $data)
	{
		$this->db->where('id_pembayaran', $id_pem);
		return $this->db->update('pembayaran', $data);
	}
}


/* End of file Pembayaran_model.php and path \application\models\Pembayaran_model.php */
