<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Spp_model extends CI_Model
{
    public function getAllSpp()
    {
        $this->db->select('tahun, nominal');
        $this->db->where('aktif', 1);
        $this->db->order_by('tahun', 'ASC');
        return $this->db->get('ref_spp')->result();
    }

    public function save($data)
    {
        return $this->db->insert('ref_spp', $data);
    }
}


/* End of file Spp_model.php and path \application\models\Spp_model.php */
