function mengaktifkan(nisn) {
    Swal.fire({
        icon: 'question',
        title: 'Yakin?',
        text: "Yakin untuk mengaktifkan akun ini?",
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonText: 'Yakin',
        cancelButtonText: 'Batal',
        denyButtonText: `Don't save`,
    }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            $.ajax({
                url: base_url + 'Admin/Siswa/aktivasi/' + nisn + '/aktifkan',
                dataType: 'json',
                success: function(data) {
                    console.log(data)
                    if (data == true) {
                        Swal.fire(
                            'Berhasil',
                            `Berhasil mengaktifkan akun siswa`,
                            'success'
                        )
                        $(".status").html('<label class="badge badge-success">Aktif</label>')
                        $(".btn-aktif").hide()
                    }
                }
            })

        } else if (result.isDenied) {
            Swal.fire('Changes are not saved', '', 'info')
        }
    })
}

function menonaktifkan(nisn) {
    Swal.fire({
        icon: 'question',
        title: 'Yakin?',
        text: "Yakin untuk menonaktifkan akun ini?",
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonText: 'Yakin',
        cancelButtonText: 'Batal',
        denyButtonText: `Don't save`,
    }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            
            $.ajax({
                url: base_url + 'Admin/Siswa/aktivasi/' + nisn + '/nonaktifkan',
                dataType: 'json',
                success: function(data) {
                    console.log(data)
                    if (data == true) {
                        Swal.fire(
                            'Berhasil',
                            `Berhasil menonaktifkan akun siswa`,
                            'success'
                        )
                        $(".status").html('<label class="badge badge-danger">Tidak Aktif</label>')
                        $(".btn-non-aktif").hide()
                    }
                }
            })

        } else if (result.isDenied) {
            Swal.fire('Changes are not saved', '', 'info')
        }
    })
}