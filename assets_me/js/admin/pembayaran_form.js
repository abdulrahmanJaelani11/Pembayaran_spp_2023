function getSiswa(url, url2) {
	$.ajax({
		type: "POST",
		url: url,
		dataType: "json",
		data: {
			nisn: $("#nisn").val(),
		},
		success: function (data) {
			console.log(data)
			if (data.siswa.length == 0) {
				$(".detail").children().remove()
				$("#nama").val("");
				$("#kelas").val("");
				Swal.fire("Oppss", `Siswa tidak ditemukan`, "error");
			} else {
				$(".info_detail").slideDown()
				detail_siswa(data)
				$("#nama").val(data.siswa.nama);
				$("#id_siswa").val(data.siswa.id);
				$("#kelas").val(data.siswa.kelas);
			}
		},
	});

	$.ajax({
		type: "POST",
		url: url2,
		data: {
			nisn: $("#nisn").val(),
			form: true,
		},
		success: function (data) {
			console.log(data)
			$("#pembayaran").html(data);
			$("#pembayaran").attr("disabled", false);
		},
	});
}

$("#pembayaran").change(function () {
	let nisn = $("#nisn").val();

	$.ajax({
		type: "POST",
		url: base_url + "/Admin/Pembayaran/getPembayaran",
		dataType: "json",
		data: {
			id: $("#pembayaran").val(),
			nisn: nisn,
		},
		success: function (data) {
			console.log(data)
			detail_pembayaran(data)
			
			let tagihan = data[0].tagihan
			let terbayar = data[0].terbayar
			$("#tagihan").val(tagihan);

			if(terbayar != 0 || terbayar != ''){
				tagihan = tagihan - terbayar
				$("#sisa_tagihan").val(tagihan);
				$("input[name='sisa_tagihan']").val(tagihan);
				$(".form_sisa_tagihan").show();
			}else{
				$("#sisa_tagihan").val('');
				$("input[name='sisa_tagihan']").val('');
				$(".form_sisa_tagihan").hide();
				terbayar = 0
			}
			$("#terbayar").val(terbayar);
		},
	});
});

function detail_siswa(data){
	$(".detail").html(`<h5>Detail Siswa</h5>
		<table class="table table-striped table-responsive">
			<thead>
				<tr>
					<td>Nama</td>
					<td>:</td>
					<td>`+ data.siswa.nama +`</td>
				</tr>
				<tr>
					<td>NISN</td>
					<td>:</td>
					<td>`+ data.siswa.nisn +`</td>
				</tr>
				<tr>
					<td>Kelas</td>
					<td>:</td>
					<td>`+data.siswa.kelas+`</td>
				</tr>
				<tr>
					<td>Jurusan</td>
					<td>:</td>
					<td>`+data.siswa.jurusan+`</td>
				</tr>
				<tr>
					<td>JK</td>
					<td>:</td>
					<td>`+data.siswa.jenis_kelamin+`</td>
				</tr>
				<tr>
					<td>Tgl Lahir</td>
					<td>:</td>
					<td>`+data.siswa.tanggal_lahir+`</td>
				</tr>
				<tr>
					<td>Agama</td>
					<td>:</td>
					<td>`+data.siswa.agama+`</td>
				</tr>
				<tr>
					<td>No HP</td>
					<td>:</td>
					<td>`+data.siswa.no_hp+`</td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td>:</td>
					<td>`+data.siswa.alamat+`</td>
				</tr>
			</thead>
		</table>`)
}

function detail_pembayaran(data){
	let status, sisa_tagihan = data[0].tagihan, tanggal = "", metode_bayar = ""
	
	if(data[0].terbayar != ""){
		tanggal = `
		<tr>
			<td>Tanggal Bayar</td>
			<td>:</td>
			<td>`+ data[0].tgl_bayar +`</td>
		</tr>
		`
		metode_bayar = `
		<tr>
			<td>Metode Pembayaran</td>
			<td>:</td>
			<td>`+ data[0].metode_pembayaran + ` `+ data[0].bank +`</td>
		</tr>
		`
		sisa_tagihan = parseFloat(data[0].tagihan) - parseFloat(data[0].terbayar)
	}
	
	if(data[0].status == 'pending'){
		status = '<div class="badge badge-warning">Belum Lunas</div>'
	}else if(data[0].status == 'paid'){
		status = '<div class="badge badge-success">Lunas</div>'
	}


	$(".detail_pembayaran").html(`<h5>Detail SPP</h5>
	<table class="table table-striped">
		<thead>
			<tr>
				<td>Bulan</td>
				<td>:</td>
				<td>`+ data[0].bulan + ` `+ data[0].tahun +`</td>
			</tr>
			<tr>
				<td>Nominal Tagihan</td>
				<td>:</td>
				<td>`+ data[0].tagihan +`</td>
			</tr>
			<tr>
				<td>Status</td>
				<td>:</td>
				<td>`+ status +`</td>
			</tr>
			<tr>
				<td>Terbayar</td>
				<td>:</td>
				<td>`+data[0].terbayar+`</td>
			</tr>
			`+ tanggal +`
			`+ metode_bayar +`
			<tr>
				<td>Sisa Tagihan</td>
				<td>:</td>
				<td>`+ sisa_tagihan +`</td>
			</tr>
		</thead>
	</table>`)
}

function validasi() { 
	if($("#nisn").val() == ''){
		Swal.fire(
			'Opps..',
			`Silahkan masukan NISN siswa terlebih dahulu!`,
			'error'
		)
		$("#nisn").addClass('is-invalid')
		return false
	}else if($("#pembayaran").val() == null || $("#pembayaran").val() == 0){
		Swal.fire(
			'Opps..',
			`Silahkan pilih pembayaran terlebih dahulu!`,
			'error'
		)
		$("#pembayaran").addClass('is-invalid')
		return false
	}else if($("#tanggal").val() == ''){
		Swal.fire(
			'Opps..',
			`Tanggal tidak boleh kosong!`,
			'error'
		)
		$("#tanggal").addClass('is-invalid')
		return false
	}else if($("#nominal").val() == ''){
		Swal.fire(
			'Opps..',
			`Silahkan masukan nominal pembayaran terlebih dahulu!`,
			'error'
		)
		$("#nominal").addClass('is-invalid')
		return false
	}
	if($("input[name='sisa_tagihan']").val() != ''){
		if(parseInt($("#nominal").val()) > parseInt($("input[name='sisa_tagihan']").val())){
			Swal.fire(
				'Opps..',
				`Pembayaran melebihi sisa tagihan!`,
				'error'
			)
			$("#nominal").addClass('is-invalid')
			return false
		}
	}
}

$("input").click(function () { 
	$(this).removeClass('is-invalid')
 })
